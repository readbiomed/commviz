import mysql.connector
import datetime, math
from flask import Flask
from datetime import timedelta
from flask import make_response, request, current_app
from flask import stream_with_context, request, Response
from functools import update_wrapper
import networkx as nx

try:
        unicode = unicode
except NameError: #'unicode' is undefined, must be Python 3
        str = str
        unicode = str
        bytes = bytes
        basestring = (str,bytes)
else: #'unicode' exists, must be Python 2
        str = str
        unicode = unicode
        bytes = str
        basestring = basestring

#Flask configurations
app = Flask(__name__)
def crossdomain(origin=None, methods=None, headers=None,
                max_age=21600, attach_to_all=True,
                automatic_options=True):
    if methods is not None:
        methods = ', '.join(sorted(x.upper() for x in methods))
    if headers is not None and not isinstance(headers, basestring):
        headers = ', '.join(x.upper() for x in headers)
    if not isinstance(origin, basestring):
        origin = ', '.join(origin)
    if isinstance(max_age, timedelta):
        max_age = max_age.total_seconds()

    def get_methods():
        if methods is not None:
            return methods

        options_resp = current_app.make_default_options_response()
        return options_resp.headers['allow']

    def decorator(f):
        def wrapped_function(*args, **kwargs):
            if automatic_options and request.method == 'OPTIONS':
                resp = current_app.make_default_options_response()
            else:
                resp = make_response(f(*args, **kwargs))
            if not attach_to_all and request.method != 'OPTIONS':
                return resp

            h = resp.headers

            h['Access-Control-Allow-Origin'] = origin
            h['Access-Control-Allow-Methods'] = get_methods()
            h['Access-Control-Max-Age'] = str(max_age)
            if headers is not None:
                h['Access-Control-Allow-Headers'] = headers
            return resp

        f.provide_automatic_options = False
        return update_wrapper(wrapped_function, f)
    return decorator

#SQL queries
def create_having_condition_multiplicity(multiplicityString, varName):
    mult = multiplicityString.replace("%20", " ").replace("messages", "").replace("message", "").replace("greater", "").replace("than", "")
    filter_multiplicity_lower = 1
    filter_multiplicity_upper = 2
    multiplicity_having_condition = ""
    if mult.find("to") > 0:
        filter_multiplicity_lower = mult.split('to')[0].strip()
        filter_multiplicity_upper = mult.split('to')[1].strip()
        if filter_multiplicity_upper == "any":
            multiplicity_having_condition = " HAVING " + varName + " >= " + filter_multiplicity_lower
        else:
            multiplicity_having_condition = " HAVING " + varName + " >= " + filter_multiplicity_lower + " AND " + varName + " < " + filter_multiplicity_upper
    else:
        filter_multiplicity_lower = mult.strip()
        if int(mult.strip()) > 1:
            multiplicity_having_condition = " HAVING " + varName + " >= " + filter_multiplicity_lower
        else:
            multiplicity_having_condition = " HAVING " + varName + " = " + filter_multiplicity_lower
    return multiplicity_having_condition

def querying_db_get_contexts(cursor, filter):
    query = ("SELECT id, value FROM context")
    cursor.execute(query)
    results = cursor.fetchall()
    return results

def querying_db_get_node_single(cursor, filter, communicatorId=None, context=None, time=None):
    if filter!='':
        filter_aux = filter.split(',')
        filter_time = filter_aux[0]
        filter_yy = filter_aux[1]
        filter_mm = filter_aux[2]
        filter_dd = filter_aux[3]
        filter_hh = filter_aux[4]
        filter_context = filter_aux[6]
        
        messageCounts = ", 0, 0, 0, 0 "
        # axis = 1,4: senders
        # axis = 2,3: recipients
        
        query = ("SELECT communicator.id, context.id, 1, communicator.communicatorId" + messageCounts + "FROM communicator, context WHERE communicator.communicatorId='" + communicatorId + "' AND (communicator.role ='s' OR communicator.role ='sr') AND context.id='" + context + "' UNION SELECT communicator.id, context.id, 2, communicator.communicatorId" + messageCounts + "FROM communicator, context WHERE communicator.communicatorId='" + communicatorId + "' AND (communicator.role ='r' OR communicator.role ='sr') AND context.id='" + context + "' UNION SELECT communicator.id, value, 3, communicator.communicatorId" + messageCounts + "FROM communicator, timeframe WHERE communicator.communicatorId='" + communicatorId + "' AND value='" + time + "' AND timeframe.type='" + filter_time + "' AND (communicator.role ='r' OR communicator.role ='sr') UNION SELECT communicator.id, value, 4, communicator.communicatorId" + messageCounts + "FROM communicator, timeframe, axis WHERE communicator.communicatorId='" + communicatorId + "' AND value='" + time + "' AND timeframe.type='" + filter_time + "' AND (communicator.role ='s' OR communicator.role ='sr')")
    
    cursor.execute(query)
    results = cursor.fetchall()
    return results

def querying_db_get_nodes_batch(cursor, filter):
    if filter!='':
        filter_aux = filter.split(',')
        filter_time = filter_aux[0]
        filter_yy = filter_aux[1]
        filter_mm = filter_aux[2]
        filter_dd = filter_aux[3]
        filter_hh = filter_aux[4]
        filter_context = filter_aux[6]

        messageCounts = ", 0, 0, 0, 0 "
        # axis = 1,4: senders
        # axis = 2,3: recipients
        
        if filter_context != '':
            query = ("SELECT communicator.id, context.id, 1, communicator.communicatorId" + messageCounts + "FROM communicator, context WHERE (communicator.role ='s' OR communicator.role ='sr') AND context.id='"+filter_context+"'" + " UNION SELECT communicator.id, context.id, 2, communicator.communicatorId" + messageCounts + "FROM communicator, context WHERE (communicator.role ='r' OR communicator.role ='sr') AND context.id='"+filter_context+"'" + " UNION SELECT communicator.id, value, 3, communicator.communicatorId" + messageCounts + "FROM communicator, timeframe WHERE timeframe.type = '" + filter_time + "' AND (communicator.role ='r' OR communicator.role ='sr') UNION SELECT communicator.id, value, 4, communicator.communicatorId" + messageCounts + "FROM communicator, timeframe WHERE timeframe.type = '" + filter_time + "' AND (communicator.role ='s' OR communicator.role ='sr')")
        else:
            query = ("SELECT communicator.id, context.id, 1, communicator.communicatorId" + messageCounts + "FROM communicator, context WHERE (communicator.role ='s' OR communicator.role ='sr')" + " UNION SELECT communicator.id, context.id, 2, communicator.communicatorId" + messageCounts + "FROM communicator, context WHERE (communicator.role ='r' OR communicator.role ='sr') UNION SELECT communicator.id, value, 3, communicator.communicatorId" + messageCounts + "FROM communicator, timeframe WHERE timeframe.type = '" + filter_time + "' AND (communicator.role ='r' OR communicator.role ='sr') UNION SELECT communicator.id, value, 4, communicator.communicatorId" + messageCounts + "FROM communicator, timeframe WHERE timeframe.type = '" + filter_time + "' AND (communicator.role ='s' OR communicator.role ='sr')")
    cursor.execute(query)
    results = cursor.fetchall()
    return results

def querying_db_get_no_users_communications(cursor, filter):
    if filter!='':
        filter_aux = filter.split(',')
        filter_time = filter_aux[0]
        filter_yy = filter_aux[1]
        filter_mm = filter_aux[2]
        filter_dd = filter_aux[3]
        filter_hh = filter_aux[4]
        filter_context = filter_aux[6]
       	multiplicity_having_condition = create_having_condition_multiplicity(filter_aux[7], varName="numCommunications")
        
        if filter_time == 'yy':
            if filter_context != '':
            	query = ("SELECT COUNT(DISTINCT sender), COUNT(DISTINCT recipient), SUM(numCommunications) FROM (SELECT sender, recipient, COUNT(DISTINCT id) AS numCommunications FROM communication WHERE context='"+filter_context+"'" + " GROUP BY sender, recipient" + multiplicity_having_condition + ") AS x")
            else:
            	query = ("SELECT COUNT(DISTINCT sender), COUNT(DISTINCT recipient), SUM(numCommunications) FROM (SELECT sender, recipient, COUNT(DISTINCT id) AS numCommunications FROM communication GROUP BY sender, recipient" + multiplicity_having_condition + ") AS x")
        elif filter_time == 'mm':
            if filter_context != '':
            	query = ("SELECT COUNT(DISTINCT sender), COUNT(DISTINCT recipient), SUM(numCommunications) FROM (SELECT sender, recipient, COUNT(DISTINCT id) AS numCommunications FROM communication WHERE context='"+filter_context+"' AND year='"+filter_yy+"'" + " GROUP BY sender, recipient" + multiplicity_having_condition + ") AS x")
            else:
            	query = ("SELECT COUNT(DISTINCT sender), COUNT(DISTINCT recipient), SUM(numCommunications) FROM (SELECT sender, recipient, COUNT(DISTINCT id) AS numCommunications FROM communication WHERE year='"+filter_yy+"'" + " GROUP BY sender, recipient" + multiplicity_having_condition + ") AS x")
        elif filter_time == 'dd':
            if filter_context != '':
            	query = ("SELECT COUNT(DISTINCT sender), COUNT(DISTINCT recipient), SUM(numCommunications) FROM (SELECT sender, recipient, COUNT(DISTINCT id) AS numCommunications FROM communication WHERE context='"+filter_context+"' AND year='"+filter_yy+"' AND month='"+filter_mm+"'" + " GROUP BY sender, recipient" + multiplicity_having_condition + ") AS x")
            else:
            	query = ("SELECT COUNT(DISTINCT sender), COUNT(DISTINCT recipient), SUM(numCommunications) FROM (SELECT sender, recipient, COUNT(DISTINCT id) AS numCommunications FROM communication WHERE year='"+filter_yy+"' AND month='"+filter_mm+"'" + " GROUP BY sender, recipient" + multiplicity_having_condition + ") AS x")
        elif filter_time == 'hh':
            if filter_context != '':
            	query = ("SELECT COUNT(DISTINCT sender), COUNT(DISTINCT recipient), SUM(numCommunications) FROM (SELECT sender, recipient, COUNT(DISTINCT id) AS numCommunications FROM communication WHERE context='"+filter_context+"' AND year='"+filter_yy+"' AND month='"+filter_mm+"' AND day='"+filter_dd+"'" + " GROUP BY sender, recipient" + multiplicity_having_condition + ") AS x")
            else:
            	query = ("SELECT COUNT(DISTINCT sender), COUNT(DISTINCT recipient), SUM(numCommunications) FROM (SELECT sender, recipient, COUNT(DISTINCT id) AS numCommunications FROM communication WHERE year='"+filter_yy+"' AND month='"+filter_mm+"' AND day='"+filter_dd+"'" + " GROUP BY sender, recipient" + multiplicity_having_condition + ") AS x")
        elif filter_time == 'mi':
            if filter_context != '':
            	query = ("SELECT COUNT(DISTINCT sender), COUNT(DISTINCT recipient), SUM(numCommunications) FROM (SELECT sender, recipient, COUNT(DISTINCT id) AS numCommunications FROM communication WHERE context='"+filter_context+"' AND year='"+filter_yy+"' AND month='"+filter_mm+"' AND day='"+filter_dd+"' AND hour='"+filter_hh+"'" + " GROUP BY sender, recipient" + multiplicity_having_condition + ") AS x")
            else:
            	query = ("SELECT COUNT(DISTINCT sender), COUNT(DISTINCT recipient), SUM(numCommunications) FROM (SELECT sender, recipient, COUNT(DISTINCT id) AS numCommunications FROM communication WHERE year='"+filter_yy+"' AND month='"+filter_mm+"' AND day='"+filter_dd+"' AND hour='"+filter_hh+"'" + " GROUP BY sender, recipient" + multiplicity_having_condition + ") AS x")
    
    cursor.execute(query)
    results = cursor.fetchall()
    return results

def querying_db_get_edges(cursor, filter):
    if filter!='':
        filter_aux = filter.split(',')
        filter_time = filter_aux[0]
        filter_yy = filter_aux[1]
        filter_mm = filter_aux[2]
        filter_dd = filter_aux[3]
        filter_hh = filter_aux[4]
        filter_context = filter_aux[6]
       	multiplicity_having_condition = create_having_condition_multiplicity(filter_aux[7], varName="numEmails")

        if filter_time=='yy':
            if filter_context != '':
                query = ("SELECT context.id, sender, year, recipient, COUNT(DISTINCT communication.id) AS numEmails FROM communication INNER JOIN context ON communication.context+1=context.id WHERE context.id='"+filter_context+"' GROUP BY communication.context, sender, year, recipient" + multiplicity_having_condition)
            else:
                query = ("SELECT context.id, sender, year, recipient, COUNT(DISTINCT communication.id) AS numEmails FROM communication INNER JOIN context ON communication.context+1=context.id GROUP BY communication.context, sender, year, recipient" + multiplicity_having_condition)
        elif filter_time=='mm':
            if filter_context != '':
                query = ("SELECT context.id, sender, month, recipient, COUNT(DISTINCT communication.id) AS numEmails, year FROM communication INNER JOIN context ON communication.context+1=context.id WHERE year='"+filter_yy+"' and context.id='"+filter_context+"' GROUP BY communication.context, sender, year, month, recipient" + multiplicity_having_condition)
            else:
                query = ("SELECT context.id, sender, month, recipient, COUNT(DISTINCT communication.id) AS numEmails, year FROM communication INNER JOIN context ON communication.context+1=context.id WHERE year='"+filter_yy+"' GROUP BY communication.context, sender, year, month, recipient" + multiplicity_having_condition)
        elif filter_time=='dd':
            if filter_context != '':
                query = ("SELECT context.id, sender, day, recipient, COUNT(DISTINCT communication.id) AS numEmails, year, month FROM communication INNER JOIN context ON communication.context+1=context.id WHERE year='"+filter_yy+"' and month='"+filter_mm+"' and context.id='"+filter_context+"' GROUP BY communication.context, sender, year, month, day, recipient" + multiplicity_having_condition)
            else:
                query = ("SELECT context.id, sender, day, recipient, COUNT(DISTINCT communication.id) AS numEmails, year, month FROM communication INNER JOIN context ON communication.context+1=context.id WHERE year='"+filter_yy+"' and month='"+filter_mm+"' GROUP BY communication.context, sender, year, month, day, recipient" + multiplicity_having_condition)
        elif filter_time=='hh':
            if filter_context != '':
                query = ("SELECT context.id, sender, hour, recipient, COUNT(DISTINCT communication.id) AS numEmails, year, month, day FROM communication INNER JOIN context ON communication.context+1=context.id WHERE year='"+filter_yy+"' and month='"+filter_mm+"' and day='"+filter_dd+"' and context.id='"+filter_context+"' GROUP BY communication.context, sender, year, month, day, hour, recipient" + multiplicity_having_condition)
            else:
                query = ("SELECT context.id, sender, hour, recipient, COUNT(DISTINCT communication.id) AS numEmails, year, month, day  FROM communication INNER JOIN context ON communication.context+1=context.id WHERE year='"+filter_yy+"' and month='"+filter_mm+"' and day='"+filter_dd+"' GROUP BY communication.context, sender, year, month, day, hour, recipient" + multiplicity_having_condition)
        elif filter_time=='mi':
            if filter_context != '':
                query = ("SELECT context.id, sender, minute, recipient, COUNT(DISTINCT communication.id) AS numEmails, year, month, day, hour FROM communication INNER JOIN context ON communication.context+1=context.id WHERE year='"+filter_yy+"' and month='"+filter_mm+"' and day='"+filter_dd+"' and hour='"+filter_hh+"' and context.id='"+filter_context+"' GROUP BY communication.context, sender, year, month, day, hour, minute, recipient" + multiplicity_having_condition)
            else:
                query = ("SELECT context.id, sender, minute, recipient, COUNT(DISTINCT communication.id) AS numEmails, year, month, day, hour FROM communication INNER JOIN context ON communication.context+1=context.id WHERE year='"+filter_yy+"' and month='"+filter_mm+"' and day='"+filter_dd+"' and hour='"+filter_hh+"' GROUP BY communication.context, sender, year, month, day, hour, minute, recipient" + multiplicity_having_condition)

    cursor.execute(query)
    results = cursor.fetchall()
    return results

class Node:
    nodCount = 0
    def __init__(self, group, type, index, userId, sentCount_group, receivedCount_group, sentCount_overall, receivedCount_overall, nodeId):
        self.group = group
        self.type = type
        self.connectors = []
        self.index = index
        self.userId = userId
        self.sentCount_group = sentCount_group
        self.receivedCount_group = receivedCount_group
        self.sentCount_overall = sentCount_overall
        self.receivedCount_overall = receivedCount_overall
        self.nodeId = nodeId
        Node.nodCount += 1

    def displayCount(self):
        print ("Total Nodes %d" % Node.nodCount)

    def displayNode(self):
        print ("index: ",self.index,"User Id : ", self.userId,  ", Group: ", self.group, ", Type: ", self.type, ", # Connectors:",len(self.connectors))

    def add_connectors(self, node):
        self.connectors.append(node)

class Edge:
    edgeCount = 0
    def __init__(self,content,time,source,target,num_email):
        self.content = content
        self.time = time
        self.source = source
        self.target = target
        self.degree = 0
        self.numEmails = num_email
        Edge.edgeCount += 1

    def displayCount(self):
        print ("Total Edges %d" % Edge.edgeCount)

    def displayEdge(self):
        print ("content: ", self.content, ", Time: ", self.time, ", Source: ", self.source.personId,"-",len(self.source.connectors), ", Target: ", self.target.personId,"-",len(self.target.connectors))

def get_key(node):
    return "userid_" + str(node.userId) + "_group_" + str(node.group) + "_type_" + str(node.type)

def get_edge(nodeList, sender_id, target_id, content, time, group_source, group_target, source_type, target_type, num_email):
	source = None
	target = None
	key_source = "userid_" + str(sender_id) + "_group_" + str(group_source) + "_type_" + str(source_type)
	key_target = "userid_" + str(target_id) + "_group_" + str(group_target) + "_type_" + str(target_type)
	if nodeList.get(key_source):
		source = nodeList.get(key_source)
	if nodeList.get(key_target):
		target = nodeList.get(key_target)
	if source != None:
		if target != None:
			return Edge(content, time, source, target, num_email)
		else:
			return None
	else:
		return None

def getNodeJSON2(node):
    string = "{"
    string += "\"nodeId\": " + str(node.nodeId) + ","
    string += "\"userId\": \"" + str(node.userId) + "\","
    string += "\"group\": \"" + str(node.group) + "\","
    string += "\"index\": " + str(node.index) + ","
    string += "\"sentCount_group\": " + str(node.sentCount_group) + ","
    string += "\"receivedCount_group\": " + str(node.receivedCount_group) + ","
    string += "\"sentCount_overall\": " + str(node.sentCount_overall) + ","
    string += "\"receivedCount_overall\": " + str(node.receivedCount_overall) + ","
    string += "\"type\": \"" + str(node.type) + "\""
    string += "}"
    return string

def getEdgeJSON(edge):
    string = "{"
    string += "\"degree\": " + str(edge.degree) + ","
    string += "\"numEmails\": " + str(edge.numEmails) + ","
    string += "\"source\":" + str(edge.source.nodeId) + ","
    string += "\"target\":" + str(edge.target.nodeId)
    string += "}"
    return string

def getContextJSON(id, context):
    string = "{"
    string += "\"id\": \"" + str(id) + "\","
    string += "\"context\": \"" + context.encode('ascii','ignore') + "\""
    string += "}"
    return string

def getAuxJSON(num):
    string = "{"
    string += "\"num\": \"" + str(num) + "\""
    string += "}"
    return string

def getAuxJSON_sender(num):
    string = "{"
    string += "\"num\": \"" + str(num) + "\""
    string += "}"
    return string

def getAuxJSON_recipient(num):
    string = "{"
    string += "\"num\": \"" + str(num) + "\""
    string += "}"
    return string

def get_json_string_context_words(cursor):
    words_context = querying_db_get_contexts(cursor, '')
    ite = 0
    stringContextJson = "["
    for each_context in words_context:
        ite += 1
        stringContextJson += getContextJSON(each_context[0], each_context[1])
        if ite < len(words_context):
             stringContextJson += ","
    stringContextJson += "]"
    return stringContextJson

def get_nodes(cursor, filter, contentlist, timelist):
    filter_aux = filter.split(',')
    nodes = []
    nodes = querying_db_get_nodes_batch(cursor, filter)
    string_nodes, nodeList = get_json_string_nodes(nodes, contentlist, timelist)
    return string_nodes, nodeList

def get_single_node(cursor, filter, person, time, context):
    nodes = []
    nodes = querying_db_get_node_single(cursor, filter=filter, communicatorId=person, context=context, time=time)
    return nodes

def get_json_string_nodes(nodes, contentlist=None, timelist=None):
    #generating the nodes
    nodeList = {}
    nodeListSort = []
    ite = 0
    s = datetime.datetime.now()
    for each_node in nodes:
        # filter the nodes that are not in the list.
        if ((len(nodes)>10000) or (timelist is None) or (contentlist is None) or (((unicode(each_node[3]), int(each_node[1])) in contentlist and each_node[2] in (1,2))) or (((unicode(each_node[3]), int(each_node[1])) in timelist and each_node[2] in (3,4)))):
            newNode = Node(each_node[1], each_node[2], 0, each_node[3], each_node[4], each_node[5], each_node[6], each_node[7], ite)
            nodeAux = [get_key(newNode), each_node[2], each_node[1], newNode.group]
            nodeListSort.append(nodeAux)
            nodeList[get_key(newNode)] = newNode
            ite +=1
            
    e = datetime.datetime.now()
    print "time:",e-s
    #sort by userid and type
    nodeListSort.sort(key=lambda x: (int(x[1]),int(x[3])),reverse=False)
    
    ite = 0
    stringNodeJson = "["
    lastGroup = ""
    lastType = ""
    index = 0

    #calculate the index, which determines the position in axis
    count = 0
    axis1_count = 0
    axis2_count = 0
    axis3_count = 0
    axis4_count = 0
    for each_node in nodeListSort:
    	if (each_node[1] == 1):
    	    axis1_count += 1
    	if (each_node[1] == 2):
    	    axis2_count += 1
    	if (each_node[1] == 3):
    	    axis3_count += 1
    	if (each_node[1] == 4):
    	    axis4_count += 1
    max_axis_count = 0
    if (axis1_count > max_axis_count):
    	max_axis_count = axis1_count
    if (axis2_count > max_axis_count):
    	max_axis_count = axis2_count
    if (axis3_count > max_axis_count):
    	max_axis_count = axis3_count
    if (axis4_count > max_axis_count):
    	max_axis_count = axis4_count
    
    for each_node in nodeListSort:
        ite += 1
        if unicode(each_node[1]) != unicode(lastType):
            index = 0 #starting from index=0 on each new axis
            lastType = each_node[1]
            lastGroup = each_node[2]
            nodeList[each_node[0]].index = index
        else:
            if unicode(each_node[2]) != unicode(lastGroup):
                lastGroup = each_node[2]
            	if (each_node[1] == 1):
                    index += max_axis_count/axis1_count
            	if (each_node[1] == 2):
                    index += max_axis_count/axis2_count
            	if (each_node[1] == 3):
                    index += max_axis_count/axis3_count
            	if (each_node[1] == 4):
                    index += max_axis_count/axis4_count
            else:
            	if (each_node[1] == 1):
                    index += max_axis_count/axis1_count
            	if (each_node[1] == 2):
                    index += max_axis_count/axis2_count
            	if (each_node[1] == 3):
                    index += max_axis_count/axis3_count
            	if (each_node[1] == 4):
                    index += max_axis_count/axis4_count
            nodeList[each_node[0]].index = index
        nodeList[each_node[0]].nodeId = count
        stringNodeJson += getNodeJSON2(nodeList[each_node[0]])
        count += 1
        if ite < len(nodeListSort):
            stringNodeJson += ","

    stringNodeJson += "]" #closing object nodes
    return stringNodeJson, nodeList

#query and process all the information
def get_nodes_and_edges(cursor, filter):
    start_time = datetime.datetime.now()
    print "START:", (start_time)

    edges_db = ''
    edges_db = querying_db_get_edges(cursor, filter)
    total_senders_recipients_communications = querying_db_get_no_users_communications(cursor, filter)
    total_senders = total_senders_recipients_communications[0][0]
    total_recipients = total_senders_recipients_communications[0][1]
    total_emails = total_senders_recipients_communications[0][2]

    stringEdgeJson, stringNodeJson = get_json_string_edges(cursor, edges_db, filter, total_senders, total_recipients)
    stringContextJson = get_json_string_context_words(cursor)
    stringAuxJson = getAuxJSON(total_emails)
    stringAuxJson_sender = getAuxJSON_sender(total_senders)
    stringAuxJson_recipient = getAuxJSON_recipient(total_recipients)

    stringResult = "{ \"nodes\":"
    stringResult += stringNodeJson
    stringResult += ",\"links\":" + stringEdgeJson
    stringResult += ",\"contexts\":" + stringContextJson
    stringResult += ",\"totalEmails\":" + stringAuxJson
    stringResult += ",\"totalSenders\":" + stringAuxJson_sender
    stringResult += ",\"totalRecipients\":" + stringAuxJson_recipient
    stringResult += ",\"totalUniqueEdges\":" + str(len(edges_db))
    stringResult += "}"
    end_time = datetime.datetime.now()
    print "END:", (end_time)
    print ("Duration of processing: ", end_time - start_time)
    return stringResult

def get_json_string_edges(cursor, edges_db, filter, total_senders, total_recipients):
    edges_list = []
    contentlist = []
    timelist = []
    nodes = []
    # compare the number of edges with threshold,
    # to decide query all nodes and filter it,
    # or query all single nodes.
    if len(edges_db) > 300:
        for each_email in edges_db:
            content = each_email[0]
            sender_id = each_email[1]
            time = each_email[2]
            target_id = each_email[3]
            if sender_id!='' and target_id!='' :
                #put node into nodelist for querying
                if (sender_id, content) not in contentlist :
                    contentlist.append((sender_id, content))
                    
                if (sender_id, int(time)) not in timelist :
                    timelist.append((sender_id, int(time)))
                    
                if (target_id, content) not in contentlist :
                    contentlist.append((target_id, content))
                    
                if (target_id, int(time)) not in timelist :
                    timelist.append((target_id, int(time)))

        stringNodeJson, nodeList = get_nodes(cursor, filter, contentlist, timelist)
    else:
        #query single nodes
        for each_email in edges_db:
        	content = each_email[0]
        	sender_id = each_email[1]
        	time = each_email[2]
        	target_id = each_email[3]
        	num_emails = each_email[4]
        	if sender_id!='' and target_id!='' and content != '' and time != '':
        		x_sn = get_single_node(cursor, filter, sender_id, str(time), str(content))
            	if x_sn:
            		nodes.extend(x_sn)
            	y_sn = get_single_node(cursor, filter, target_id, str(time), str(content))
            	if y_sn:
            		nodes.extend(y_sn)
        stringNodeJson, nodeList = get_json_string_nodes(nodes)
    
    #add edge to edge list
    for each_email in edges_db:
        context = each_email[0]
        sender_id = each_email[1]
        time = each_email[2]
        target_id = each_email[3]
        num_emails = each_email[4]
        if sender_id != '' and target_id != '' and context != '' and time != '':
        	x13 = get_edge(nodeList, sender_id, target_id, context, time, context, time, 1, 3, num_emails)
        	x43 = get_edge(nodeList, sender_id, target_id, context, time, time, time, 4, 3, num_emails)
        	x12 = get_edge(nodeList, sender_id, target_id, context, time, context, context, 1, 2, num_emails)
        	if x13:
        		edges_list.append(x13)
        	if x43:
        		edges_list.append(x43)
        	if x12:
        		edges_list.append(x12)
    
    ite = 0
    stringEdgeJson = "["
    for each_edge in edges_list:
        ite += 1
        stringEdgeJson += getEdgeJSON(each_edge)
        if ite < len(edges_list):
             stringEdgeJson += ","
    stringEdgeJson += "]"
    return stringEdgeJson, stringNodeJson

##Network structure functions ----------------------------------------------------------------------------------------------------------
def get_nodes_and_edges_network(cursor, filter, axis, index):
    start_time = datetime.datetime.now()
    print "START:", (start_time)
    edges_db = ''
    edges_db = querying_db_get_edges(cursor, filter)
    total_senders_recipients_communications = querying_db_get_no_users_communications(cursor, filter)
    total_senders = total_senders_recipients_communications[0][0]
    total_recipients = total_senders_recipients_communications[0][1]
    total_emails = total_senders_recipients_communications[0][2]

    stringEdgeJson, stringNodeJson = get_json_string_edges_network(edges_db, filter, axis, index)
    stringAuxJson = getAuxJSON(total_emails)
    stringAuxJson_sender = getAuxJSON_sender(total_senders)
    stringAuxJson_recipient = getAuxJSON_recipient(total_recipients)

    stringResult = "{ \"nodes\":"
    stringResult += stringNodeJson
    stringResult += ",\"links\":" + stringEdgeJson
    stringResult += ",\"totalEmails\":"+ stringAuxJson
    stringResult += ",\"totalSenders\":" + stringAuxJson_sender
    stringResult += ",\"totalRecipients\":" + stringAuxJson_recipient
    stringResult += ",\"totalUniqueEdges\":" + str(len(edges_db))
    stringResult += "}"
    end_time = datetime.datetime.now()
    print "END:", (end_time)
    print ("Duration of processing: ", end_time - start_time)
    return stringResult

def get_json_string_edges_network(edges_db, filter, axis, index):
    edges_list = []
    G=nx.Graph()
    axisdict={}
    indexdict={}
    nodes=[]
    ranges=[]
    grouprange=[[],[],[]]
    #traverse all edges and add into graph
    for each_email in edges_db:
        content = each_email[0] #0
        sender_id = each_email[1] #1
        time = each_email[2] #2
        target_id = each_email[3] #3
        num_emails = each_email[4]
        G.add_edge(str(sender_id),str(target_id),weight=num_emails)
    #calculate CC DEG or PR value based on requirement
    if axis=='cc':
        axisdict=nx.clustering(G)
    elif axis=='deg':
        axisdict=nx.degree(G)
    elif axis=='pr':
        axisdict=nx.pagerank(G)
    if index=='cc':
        indexdict=nx.clustering(G)
    elif index=='deg':
        indexdict=nx.degree(G)
    elif index=='pr':
        indexdict=nx.pagerank(G)

    #get the range to divide axis
    if axis == 'cc':
        CCRANGE=[0.00000000001,0.9999999999]
        ranges=CCRANGE
    else:
        ranges=get_ranges(axisdict)

    #put each node on the axis
    for node in axisdict: 
        if axisdict[node]<ranges[0]:
            nodes.append([0,node,indexdict[node]])
            nodes.append([1,node,indexdict[node]])
            if indexdict[node] not in grouprange[0]:
                grouprange[0].append(indexdict[node])
        elif axisdict[node]>= ranges[0] and axisdict[node]<=ranges[1]:
            nodes.append([2,node,indexdict[node]])
            nodes.append([3,node,indexdict[node]])
            if indexdict[node] not in grouprange[1]:
                grouprange[1].append(indexdict[node])      
        elif axisdict[node]>ranges[1]:
            nodes.append([4,node,indexdict[node]])
            nodes.append([5,node,indexdict[node]])
            if indexdict[node] not in grouprange[2]:
                grouprange[2].append(indexdict[node])

    #get JSON string of nodes
    stringNodeJson, nodeList = get_json_string_nodes_network(nodes, grouprange)

    #get JSON string of edges
    for each_email in edges_db:
        content = each_email[0]
        sender_id = each_email[1]
        time = each_email[2]
        target_id = each_email[3]
        num_emails = each_email[4]
        if sender_id!='' and target_id!='' and content != '' and time != '':
            #decide which two axis the edge connected
            if axisdict[sender_id]<ranges[0]:
                if axisdict[target_id]<ranges[0]:
                    edges_list.append(get_edge_network(nodeList,sender_id,target_id,content,time,0,1,num_emails))
                elif axisdict[target_id]>=ranges[0] and axisdict[target_id]<=ranges[1]:
                    edges_list.append(get_edge_network(nodeList,sender_id,target_id,content,time,1,2,num_emails))
                elif axisdict[target_id]>ranges[1]:
                    edges_list.append(get_edge_network(nodeList,sender_id,target_id,content,time,0,5,num_emails))
            elif axisdict[sender_id]>= ranges[0] and axisdict[sender_id]<=ranges[1]:
                if axisdict[target_id]<ranges[0]:
                    edges_list.append(get_edge_network(nodeList,sender_id,target_id,content,time,2,1,num_emails))
                elif axisdict[target_id]>=ranges[0] and axisdict[target_id]<=ranges[1]:
                    edges_list.append(get_edge_network(nodeList,sender_id,target_id,content,time,2,3,num_emails))
                elif axisdict[target_id]>ranges[1]:
                    edges_list.append(get_edge_network(nodeList,sender_id,target_id,content,time,3,4,num_emails))
            elif axisdict[sender_id]>ranges[1]:
                if axisdict[target_id]<ranges[0]:
                    edges_list.append(get_edge_network(nodeList,sender_id,target_id,content,time,5,0,num_emails))
                elif axisdict[target_id]>= ranges[0] and axisdict[target_id]<=ranges[1]:
                    edges_list.append(get_edge_network(nodeList,sender_id,target_id,content,time,4,3,num_emails))
                elif axisdict[target_id]>ranges[1]:
                    edges_list.append(get_edge_network(nodeList,sender_id,target_id,content,time,4,5,num_emails))
    ite = 0
    stringEdgeJson = "["
    for each_edge in edges_list:
        ite += 1
        stringEdgeJson += getEdgeJSON(each_edge)
        if ite < len(edges_list):
             stringEdgeJson += ","
    stringEdgeJson += "]"

    return stringEdgeJson, stringNodeJson

#split the list
def splist(l,n):
    length=len(l)
    sz=length//n
    c=length%n
    lst=[]
    i=0
    while i<n:
        if i<c:
            bs=sz+1
            lst.append(l[i*bs:i*bs+bs])
        else:
            lst.append(l[i*sz+c:i*sz+c+sz])
        i+=1
    return lst

def color_range(grouprange):
    colorranges=[]
    for onerange in grouprange:
        color_range=[]
        if len(onerange)<=10:
            color_range=[t for t in sorted(onerange)]
        else :
            arr=sorted(onerange)
            color_range=splist(arr,10)
        colorranges.append(color_range)
    print colorranges
    return colorranges

def get_color(value,color_range):
    i=0
    while i<10:
        if type(color_range[i]) is list: 
            if value in color_range[i]:
                break
            else:
                i=i+1
        else:
            if (value == color_range[i]):
                break
            else:
                i=i+1
    return i
    
def getNodeJSON_network(node):
    string = "{"
    string += "\"nodeId\": " + str(node.nodeId) + ","
    string += "\"userId\": \"" + str(node.userId) + "\","
    string += "\"group\": \"" + str(node.group) + "\","
    string += "\"index\": " + str(node.index) + ","
    string += "\"value\": " + str(node.value) + ","
    string += "\"type\": \"" + str(node.type) + "\""
    string += "}"
    return string


def get_json_string_nodes_network(nodes, grouprange):
    #generating the nodes
    nodeList = {}
    nodeListSort = []
    ite = 0
    colorrange=color_range(grouprange)
    for each_node in nodes:
        newNode = Node_network(get_color(each_node[2],colorrange[each_node[0]/2]),each_node[0],0,each_node[1],ite,each_node[2])
        nodeAux = [get_key_network(newNode), each_node[0], each_node[2]]
        nodeListSort.append(nodeAux)
        nodeList[get_key_network(newNode)] = newNode
        ite +=1
    nodeListSort.sort(key=lambda x: (int(x[1]),float(x[2])),reverse=False)

    ite = 0
    stringNodeJson = "["
    lastGroup = ""
    lastType = ""
    index = 0

    count = 0
    axis1_count = 0
    axis2_count = 0
    axis3_count = 0
    axis4_count = 0
    axis5_count = 0
    axis6_count = 0
    for each_node in nodeListSort:
    	if (each_node[1] == 0):
    	    axis1_count += 1
    	if (each_node[1] == 1):
    	    axis2_count += 1
    	if (each_node[1] == 2):
    	    axis3_count += 1
    	if (each_node[1] == 3):
    	    axis4_count += 1
    	if (each_node[1] == 4):
    	    axis5_count += 1
    	if (each_node[1] == 5):
    	    axis6_count += 1
    max_axis_count = 0
    if (axis1_count > max_axis_count):
    	max_axis_count = axis1_count
    if (axis2_count > max_axis_count):
    	max_axis_count = axis2_count
    if (axis3_count > max_axis_count):
    	max_axis_count = axis3_count
    if (axis4_count > max_axis_count):
    	max_axis_count = axis4_count
    if (axis5_count > max_axis_count):
    	max_axis_count = axis5_count
    if (axis6_count > max_axis_count):
    	max_axis_count = axis6_count
    
    for each_node in nodeListSort:
        ite += 1
        if unicode(each_node[1]) != unicode(lastType):
            index = 0 #starting from index=0 on each new axis
            lastType = each_node[1]
            lastGroup = each_node[2]
            nodeList[each_node[0]].index = index
        else:
            if unicode(each_node[2]) != unicode(lastGroup):
                lastGroup = each_node[2]
            	if (each_node[1] == 01):
                    index += max_axis_count/axis1_count
            	if (each_node[1] == 1):
                    index += max_axis_count/axis2_count
            	if (each_node[1] == 2):
                    index += max_axis_count/axis3_count
            	if (each_node[1] == 3):
                    index += max_axis_count/axis4_count
            	if (each_node[1] == 4):
                    index += max_axis_count/axis5_count
            	if (each_node[1] == 5):
                    index += max_axis_count/axis6_count
            else:
            	if (each_node[1] == 0):
                    index += max_axis_count/axis1_count
            	if (each_node[1] == 1):
                    index += max_axis_count/axis2_count
            	if (each_node[1] == 2):
                    index += max_axis_count/axis3_count
            	if (each_node[1] == 3):
                    index += max_axis_count/axis4_count
            	if (each_node[1] == 4):
                    index += max_axis_count/axis5_count
            	if (each_node[1] == 5):
                    index += max_axis_count/axis6_count
            nodeList[each_node[0]].index = index
        nodeList[each_node[0]].nodeId = count
        stringNodeJson += getNodeJSON_network(nodeList[each_node[0]])
        count += 1
        if ite < len(nodeListSort):
            stringNodeJson += ","
    stringNodeJson += "]"
    return stringNodeJson, nodeList

#node class in network structure
class Node_network:
    nodCount = 0
    def __init__(self, group, type, index, userId, nodeId, value):
        self.group = group
        self.type = type
        self.connectors = []
        self.index = index
        self.userId=userId
        self.nodeId = nodeId
        self.value=value
        Node.nodCount += 1
        
def get_key_network(node):
    return "userid_" + str(node.userId) + "_type_" + str(node.type)

def get_edge_network(nodeList,sender_id,target_id,content,time,source_type,target_type,num_email):
    key_source = "userid_" + str(sender_id) + "_type_" + str(source_type)
    source = nodeList[key_source]
    key_target = "userid_" + str(target_id) + "_type_" + str(target_type)
    target = nodeList[key_target]
    edge = Edge(content,time,source,target,num_email)
    return edge

def get_ranges(axisdict):
    arr=sorted(axisdict.values())
    array=[list(t) for t in zip(*[iter(arr)]*(len(arr)/3))]
    ranges=[array[1][1],array[1][-1]]
    return ranges

#Original Commviz server functions(COPYED FROM SERVER)--------------------------------------------------------------------------------------------------------
#Most part are duplicated

def querying_db_commviz(type_query, filter):
##    start_time = datetime.datetime.now()
##    print start_time

    if filter!='':
        filter_aux = filter.split(',')
        filter_type = filter_aux[0]
        filter_yy = filter_aux[1]
        filter_mm = filter_aux[2]
        filter_dd = filter_aux[3]
        filter_hh = filter_aux[4]
        filter_topic = filter_aux[6]

    cnx = mysql.connector.connect(user='commviz-user', database='commviz', password='commviz-user')
    ##cnx = mysql.connector.connect(user='passroot', database='commviz', password='wd5158108')
    cursor = cnx.cursor()
    if type_query is 1: #returning edges
        if filter_type=='yy':
            if filter_topic != '':
                query = ("SELECT original_message_id, weight, `messages`.topic, sender, year, recipient,count(distinct original_message_id) as numEmails FROM `messages` INNER JOIN topics ON messages.topic=topics.topic WHERE messages.topic='"+filter_topic+"' group by messages.topic, sender, year, recipient")
            else:
                query = ("SELECT original_message_id, weight, `messages`.topic, sender, year, recipient,count(distinct original_message_id) as numEmails FROM `messages` INNER JOIN topics ON messages.topic=topics.topic group by messages.topic, sender, year, recipient")
        elif filter_type=='mm':
            if filter_topic != '':
                query = ("SELECT original_message_id, weight, messages.topic, sender, month, recipient,count(distinct original_message_id) as numEmails, year FROM `messages` INNER JOIN topics ON messages.topic=topics.topic WHERE year='"+filter_yy+"' and messages.topic='"+filter_topic+"' group by messages.topic, sender, year, month, recipient")
            else:
                query = ("SELECT original_message_id, weight, messages.topic, sender, month, recipient,count(distinct original_message_id) as numEmails, year FROM `messages` INNER JOIN topics ON messages.topic=topics.topic WHERE year='"+filter_yy+"' group by messages.topic, sender, year, month, recipient")
        elif filter_type=='dd':
            if filter_topic != '':
                query = ("SELECT original_message_id, weight, messages.topic, sender, day, recipient,count(distinct original_message_id) as numEmails, year, month FROM `messages` INNER JOIN topics ON messages.topic=topics.topic WHERE year='"+filter_yy+"' and month='"+filter_mm+"' and messages.topic='"+filter_topic+"' group by messages.topic, sender, year, month,day, recipient")
            else:
                query = ("SELECT original_message_id, weight, messages.topic, sender, day, recipient,count(distinct original_message_id) as numEmails, year, month FROM `messages` INNER JOIN topics ON messages.topic=topics.topic WHERE year='"+filter_yy+"' and month='"+filter_mm+"' group by messages.topic, sender, year, month,day, recipient")
        elif filter_type=='hh':
            if filter_topic != '':
                query = ("SELECT original_message_id, weight, messages.topic, sender, `hour`, recipient,count(distinct original_message_id) as numEmails, year, month, `day`  FROM `messages` INNER JOIN topics ON messages.topic=topics.topic WHERE year='"+filter_yy+"' and month='"+filter_mm+"' and day='"+filter_dd+"' and messages.topic='"+filter_topic+"' group by messages.topic, sender, `year`, month, `day`, `hour`, recipient")
            else:
                query = ("SELECT original_message_id, weight, messages.topic, sender, `hour`, recipient,count(distinct original_message_id) as numEmails, year, month, `day`  FROM `messages` INNER JOIN topics ON messages.topic=topics.topic WHERE year='"+filter_yy+"' and month='"+filter_mm+"' and day='"+filter_dd+"' group by messages.topic, sender, `year`, month, `day`, `hour`, recipient")
        elif filter_type=='mi':
            if filter_topic != '':
                query = ("SELECT original_message_id, weight, messages.topic, sender, `minute`, recipient,count(distinct original_message_id) as numEmails, year, month, `day`, `hour` FROM `messages` INNER JOIN topics ON messages.topic=topics.topic WHERE year='"+filter_yy+"' and month='"+filter_mm+"' and day='"+filter_dd+"' and hour='"+filter_hh+"' and messages.topic='"+filter_topic+"' group by messages.topic, sender, `year`, month, `day`, `hour`, `minute`, recipient")
            else:
                query = ("SELECT original_message_id, weight, messages.topic, sender, `minute`, recipient,count(distinct original_message_id) as numEmails, year, month, `day`, `hour` FROM `messages` INNER JOIN topics ON messages.topic=topics.topic WHERE year='"+filter_yy+"' and month='"+filter_mm+"' and day='"+filter_dd+"' and hour='"+filter_hh+"' group by messages.topic, sender, `year`, month, `day`, `hour`, `minute`, recipient")
    elif type_query is 2 :#returning nodes
        if filter_topic != '':
            query = ("select personid, package_group, axis.name as axis,email, people.name from people,timeframes,axis where timeframes.type = '" + filter_type + "' and axis.name in (3,4) union select personid,topic,axis.name as axis,email, people.name from people,topics,axis where axis.name in (1,2) and topic='"+filter_topic+"'")
        else:
            query = ("select personid, package_group, axis.name as axis,email, people.name from people,timeframes,axis where timeframes.type = '" + filter_type + "' and axis.name in (3,4) union select personid,topic,axis.name as axis,email, people.name from people,topics,axis where axis.name in (1,2)")
    elif type_query is 3:
        query = ("SELECT TEMP.personid, TEMP.name, TEMP.email, TEMP1.groupEnter, TEMP.groupLeave FROM (SELECT people.personid, people.name, people.email, COUNT(DISTINCT (original_message_id)) as groupLeave FROM `people` LEFT JOIN messages ON people.personid = messages.sender group by people.personid) TEMP INNER JOIN (SELECT people.personid, people.name, people.email, COUNT(DISTINCT (original_message_id)) as groupEnter FROM `people` LEFT JOIN messages ON people.personid = messages.recipient group by people.personid) TEMP1 ON TEMP.personid = TEMP1.personid")
    elif type_query is 4:
        query = ("SELECT * FROM messages WHERE sender<>'' and recipient<>'' group by sender, recipient")
    elif type_query is 5: #return words topics
        query = ("select topic,words from topics")
    elif type_query is 6: #retunr nodes = topics
        query = ("select topic,personid,axis.name as axis,email, people.name from people,topics,axis where axis.name in (1,2)")
    elif type_query is 7: #return edges of nodes_topics
        query = ("SELECT `messages`.topic,sender, sender, recipient,count(*) as numEmails FROM `messages` INNER JOIN topics ON messages.topic=topics.topic group by messages.topic, sender, recipient")
    elif type_query is 8:
        if filter_type=='yy':
            if filter_topic != '':
                query = ("SELECT count(distinct original_message_id) as numEmails FROM `messages` WHERE messages.topic='"+filter_topic+"'")
            else:
                query = ("SELECT count(distinct original_message_id) as numEmails FROM `messages`")
        elif filter_type=='mm':
            if filter_topic != '':
                query = ("SELECT count(distinct original_message_id) as numEmails FROM `messages` WHERE year='"+filter_yy+"' and messages.topic='"+filter_topic+"' ")
            else:
                query = ("SELECT count(distinct original_message_id) as numEmails FROM `messages` WHERE year='"+filter_yy+"'")
        elif filter_type=='dd':
            if filter_topic != '':
                query = ("SELECT count(distinct original_message_id) as numEmails FROM `messages` WHERE year='"+filter_yy+"' and month='"+filter_mm+"' and messages.topic='"+filter_topic+"'")
            else:
                query = ("SELECT count(distinct original_message_id) as numEmails FROM `messages` WHERE year='"+filter_yy+"' and month='"+filter_mm+"'")
        elif filter_type=='hh':
            if filter_topic != '':
                query = ("SELECT count(distinct original_message_id) as numEmails  FROM `messages` WHERE year='"+filter_yy+"' and month='"+filter_mm+"' and day='"+filter_dd+"' and messages.topic='"+filter_topic+"'")
            else:
                query = ("SELECT count(distinct original_message_id) as numEmails FROM `messages` WHERE year='"+filter_yy+"' and month='"+filter_mm+"' and day='"+filter_dd+"'")
        elif filter_type=='mi':
            if filter_topic != '':
                query = ("SELECT count(distinct original_message_id) as numEmails FROM `messages` WHERE year='"+filter_yy+"' and month='"+filter_mm+"' and day='"+filter_dd+"' and hour='"+filter_hh+"' and messages.topic='"+filter_topic+"'")
            else:
                query = ("SELECT count(distinct original_message_id) as numEmails FROM `messages` WHERE year='"+filter_yy+"' and month='"+filter_mm+"' and day='"+filter_dd+"' and hour='"+filter_hh+"'")
    cursor.execute(query)
    results = cursor.fetchall()
    cursor.close()
    cnx.close()
##    end_time = datetime.datetime.now()
##    print end_time
##    print "Duration of processing: ", end_time - start_time
    return results

def get_group_commviz(group):
    if group == 'Jan':
        return 1
    elif group == 'Feb':
        return 2
    elif group == 'Mar':
        return 3
    elif group == 'Apr':
        return 4
    elif group == 'May':
        return 5
    elif group == 'Jun':
        return 6
    elif group == 'Jul':
        return 7
    elif group == 'Aug':
        return 8
    elif group == 'Sep':
        return 9
    elif group == 'Oct':
        return 10
    elif group == 'Nov':
        return 11
    elif group == 'Dec':
        return 12
    else:
        return int(group)

class Node_Link_commviz:
    nodCount = 0
    def __init__(self,person_id,email,name,group,node_id):
        self.personId = person_id
        self.email = email
        self.name = name
        self.group = group
        self.nodeId = node_id
        Node_Link_commviz.nodCount += 1

class Node_commviz:
    nodCount = 0
    def __init__(self,personId,group,type,index,email,name,nodeId):
        self.personId = personId
        self.group = group
        self.type = type
        self.connectors = []
        self.index = index
        self.email = email
        self.name = name
        self.nodeId = nodeId
        self.group_int = get_group_commviz(group)
        Node.nodCount += 1

    def displayCount(self):
        print ("Total Nodes %d" % Node.nodCount)

    def displayNode(self):
        print ("index: ",self.index,"Person Id : ", self.personId,  ", Group: ", self.group, ", Type: ", self.type, ", # Connectors:",len(self.connectors))

    def add_connectors(self, node):
        self.connectors.append(node)

class Edge_commviz:
    edgeCount = 0
    def __init__(self,messageId,weight,topic,time,source,target,num_email):
        self.messageId = messageId
        self.weight = weight
        self.topic = topic
        self.time = time
        self.source = source
        self.target = target
        self.degree = 0
        self.numEmails = num_email
        Edge.edgeCount += 1

    def displayCount(self):
        print ("Total Edges %d" % Edge.edgeCount)

    def displayEdge(self):
        print ("Message Id : ", self.messageId,  ", Weight: ", self.weight, ", Topic: ", self.topic, ", Time: ", self.time, ", Source: ", self.source.personId,"-",len(self.source.connectors), ", Target: ", self.target.personId,"-",len(self.target.connectors))

def get_key_commviz(node):
    return "personid_" + str(node.personId) + "_group_" + str(node.group) + "_type_" + str(node.type)

def get_edge_commviz(nodeList,message_id,sender_id,target_id,weight,topic,time,group_source,group_target,source_type,target_type,num_email):
    key_source = "personid_" + str(sender_id) + "_group_" + str(group_source) + "_type_" + str(source_type)
    source = nodeList[key_source]
    key_target = "personid_" + str(target_id) + "_group_" + str(group_target) + "_type_" + str(target_type)
    target = nodeList[key_target]
    edge = Edge_commviz(message_id,weight,topic,time,source,target,num_email)
    return edge

def getNodeJSON2_commviz(node):
    string = "{" #opening object
    string += "\"nodeId\": " + str(node.nodeId) + ","
    string += "\"personId\": \"" + str(node.personId) + "\","
    string += "\"email\": \"" + str(node.email) + "\","
    string += "\"name\": \"" + str(node.name) + "\","
    string += "\"group\": \"" + str(node.group) + "\","
    string += "\"index\": " + str(node.index) + ","
    string += "\"type\": \"" + str(node.type) + "\""
    string += "}" #closing object
    return string

def getEdgeJSON_commviz(edge):
    string = "{" #opening object source
    string += "\"messageId\": \"" + str(edge.messageId) + "\","
    string += "\"degree\": " + str(edge.degree) + ","
    string += "\"numEmails\": " + str(edge.numEmails) + ","
    string += "\"source\":" + str(edge.source.nodeId) + ","
    string += "\"target\":" + str(edge.target.nodeId)
    string += "}" #closing object source
    return string

def getEdgeJSON_Node_Link_commviz(sender,recipient,weight):
    string = "{" #opening object source
    string += "\"source\":" + str(sender) + ","
    string += "\"target\":" + str(recipient) + ","
    string += "\"value\":" + str(weight)
    string += "}" #closing object source
    return string

def getNodeJSON_Node_Link_commviz(person_id,email,name,group):
    string = "{" #opening object
    string += "\"personId\":\"" + str(person_id) + "\","
    string += "\"email\":\"" + str(email) + "\","
    string += "\"name\":\"" + str(name) + "\","
    string += "\"group\": " + str(group)
    string += "}" #closing object
    return string

def getTopicsJSON_commviz(topic,words):
    string = "{" #opening object
    string += "\"topic\": \"" + str(topic) + "\","
    string += "\"words\": \"" + str(words) + "\""
    string += "}" #closing object
    return string

def getAuxJSON_commviz(num):
    string = "{" #opening object
    string += "\"num\": \"" + str(num) + "\""
    string += "}" #closing object
    return string

def get_json_string_topics_words_commviz():
    words_topics=querying_db_commviz(5,'')
    ite = 0
    stringTopicJson = "["
    for each_topic in words_topics:
        ite += 1
        stringTopicJson += getTopicsJSON_commviz(each_topic[0],each_topic[1])
        if ite < len(words_topics):
             stringTopicJson += ","
    stringTopicJson += "]" #closing object topic
    return stringTopicJson

def get_nodes_commviz(filter,type_node):

    filter_aux = filter.split(',')
    if int(type_node) is 0 : #nodes = people
        nodes = querying_db_commviz(2,filter)
    elif int(type_node) is 1 : #nodes = topics
        nodes = querying_db_commviz(6,'')

    string_nodes, nodeList = get_json_string_nodes_commviz(nodes)
    return string_nodes, nodeList

def get_json_string_nodes_commviz(nodes):
    #generating the nodes
    nodeList = {}
    nodeListSort = []
    ite = 0
    for each_node in nodes:
        if each_node[0]!='' and each_node[1]!='' and each_node[2]!='':
            newNode = Node_commviz(each_node[0],each_node[1],each_node[2],0,each_node[3],each_node[4],ite)
            nodeAux = [get_key_commviz(newNode) , each_node[2]  , each_node[1], newNode.group_int]
            nodeListSort.append(nodeAux)
            nodeList[get_key_commviz(newNode)] = newNode
            ite +=1

    nodeListSort.sort(key=lambda x: (int(x[1]),int(x[3])),reverse=False)

    ite = 0
    stringNodeJson = "["
    lastGroup = ""
    lastType = ""
    index = 0

    count = 0
    for each_node in nodeListSort:
        ite += 1
        if unicode(each_node[1]) != unicode(lastType):
            index = 0
            lastType = each_node[1]
            lastGroup = each_node[2]
            nodeList[each_node[0]].index = index
        else:
            if unicode(each_node[2]) != unicode(lastGroup):
                lastGroup = each_node[2]
                index += 2
            else:
                index += 1
            nodeList[each_node[0]].index = index

        nodeList[each_node[0]].nodeId = count
        stringNodeJson += getNodeJSON2_commviz(nodeList[each_node[0]])
        count += 1
        if ite < len(nodeListSort):
            stringNodeJson += ","
    stringNodeJson += "]" #closing object nodes

    return stringNodeJson, nodeList

def get_nodes_and_edges_commviz(filter,type_node):

    stringNodeJson, nodeList = get_nodes_commviz(filter,type_node)
    edges_db=''
    total_emails = 0
    if int(type_node) is 0:
        edges_db = querying_db_commviz(1,filter)
        total_emails = querying_db_commviz(8,filter)[0][0]
    elif int(type_node) is 1:
        edges_db = querying_db_commviz(7,'')
        total_emails = querying_db_commviz(8,'')[0][0]

    stringEdgeJson = get_json_string_edges_commviz(edges_db,type_node,nodeList)
    stringTopicsJson = get_json_string_topics_words_commviz()
    stringAuxJson = getAuxJSON_commviz(total_emails)

    stringResult = "{ \"nodes\":"
    stringResult += stringNodeJson
    stringResult += ",\"links\":" + stringEdgeJson
    stringResult += ",\"topics\":"+ stringTopicsJson
    stringResult += ",\"totalEmails\":"+ stringAuxJson
    stringResult += "}"


    return stringResult


def get_json_string_edges_commviz(edges_db,type_node,nodeList):
    edges_list = []
    #generating the list of edges
    for each_email in edges_db:
        message_id = each_email[0]
        weight = each_email[1]
        topic = each_email[2] #0
        sender_id = each_email[3] #1
        time = each_email[4] #2
        target_id = each_email[5] #3
        num_emails = each_email[6] #4

        if sender_id!='' and target_id!='' and topic != '' and time != '':
            if int(type_node) is 0:
                #edge for type1: source in axis 1 and target in axis 3
                edges_list.append(get_edge_commviz(nodeList,message_id,sender_id,target_id,weight,topic,time,topic,time,1,3,num_emails))
                #edge for type1: source in axis 3 and target in axis 4
                edges_list.append(get_edge_commviz(nodeList,message_id,sender_id,target_id,weight,topic,time,time,time,4,3,num_emails))
                #edge for type1: source in axis 2 and target in axis 1
                edges_list.append(get_edge_commviz(nodeList,message_id,sender_id,target_id,weight,topic,time,topic,topic,1,2,num_emails))
                
            elif int(type_node) is 1:
                #edge for type5: source in axis 3 and target in axis 4
                edges_list.append(get_edge_commviz(nodeList,message_id,topic,topic,weight,topic,time,sender_id,target_id,1,2,num_emails))

    ite = 0
    
    stringEdgeJson = "["
    for each_edge in edges_list:
        ite += 1
        stringEdgeJson += getEdgeJSON_commviz(each_edge)
        if ite < len(edges_list):
             stringEdgeJson += ","
    stringEdgeJson += "]" #closing object edges

    return stringEdgeJson

def get_json_string_Node_Link_commviz():

    #generating the nodes
    nodeList ={}
    nodes = querying_db_commviz(3,'')

    ite = 0
    stringNodeJson = "["
    for each_node in nodes:
        person_id = each_node[0]
        name = each_node[1]
        email = each_node[2]
        if int(each_node[3]) == 1 :
            group = 1
        elif int(each_node[3]) > 1 and int(each_node[3])<=10:
            group = 2
        elif int(each_node[3]) > 10 and int(each_node[3])<=20:
            group = 3
        elif int(each_node[3]) > 20 and int(each_node[3])<=30:
            group = 4
        elif int(each_node[3]) > 30 and int(each_node[3])<=40:
            group = 5
        elif int(each_node[3]) > 40 and int(each_node[3])<=50:
            group = 6
        elif int(each_node[3]) > 50 and int(each_node[3])<=60:
            group = 7
        elif int(each_node[3]) > 60 and int(each_node[3])<=70:
            group = 8
        elif int(each_node[3]) > 70 and int(each_node[3])<=80:
            group = 9
        elif int(each_node[3]) > 80 and int(each_node[3])<=90:
            group = 10
        elif int(each_node[3]) > 90 and int(each_node[3])<=100:
            group = 11
        elif int(each_node[3]) > 100 and int(each_node[3])<=150:
            group = 12
        elif int(each_node[3]) > 150 and int(each_node[3])<=200:
            group = 13
        elif int(each_node[3]) > 200:
            group = 14
        else:
            group = 0

        newNode = Node_Link_commviz(person_id,email,name,group,ite)

        nodeList[str(newNode.personId)] = newNode.nodeId

        ite += 1
        stringNodeJson += getNodeJSON_Node_Link_commviz(newNode.personId,newNode.email,newNode.name,newNode.group)
        if ite < len(nodes):
            stringNodeJson += ","
    stringNodeJson += "]" #closing object nodes

    ite = 0
    edges_db = querying_db_commviz(4,'')
    stringEdgeJson = "["
    for each_edge in edges_db:
        sender_id = str(each_edge[4])
        target_id = str(each_edge[11])
        weight = 1#each_edge[3]
        ite += 1
        stringEdgeJson += getEdgeJSON_Node_Link_commviz(nodeList[sender_id],nodeList[target_id],weight)
        if ite < len(edges_db):
             stringEdgeJson += ","
    stringEdgeJson += "]" #closing object edges


    stringResult = "{ \"nodes\":"
    stringResult += stringNodeJson
    stringResult += ",\"links\":" + stringEdgeJson
    stringResult += "}"
    return stringResult

@app.route("/enrondata")
@crossdomain(origin='*')
def get_data_commviz():
    s=datetime.datetime.now()
    filter = request.args.get('filter')
    if filter!='':
        filter_aux = filter.split(',')
        if int(filter_aux[7]) is 0 :
            json_result = get_nodes_and_edges_commviz(filter,0)
        elif int(filter_aux[7]) is 1:
            nodes_edges, total_emails = get_nodes_and_edges_commviz(filter,1)
            topics = get_json_string_topics_words_commviz()
            json_result = nodes_edges + "$$__$$" + topics + "$$__$$" + str(total_emails)
        elif int(filter_aux[7]) is 2:
            json_result = get_json_string_Node_Link_commviz()
    print "time usage is: ",datetime.datetime.now()-s
    return json_result

#End of original server----------------------------------------------------------------------------------------------------

#Start of VAST and Twitter-------------------------------------------------------------------------------------------------
# ALL THE FUNCTIONS WITH vt POSTFIXES NEED TO BE CHANGED UNDER THIS SECTION FOR VAST AND TWITTER
def querying_db_vt(type_query, filter,database,person=None,contents=None,time=None):
    if filter!='':
        filter_aux = filter.split(',') #split attributes
        filter_type = filter_aux[0]
        filter_yy = filter_aux[1]
        filter_mm = filter_aux[2]
        filter_dd = filter_aux[3]
        filter_hh = filter_aux[4]
        filter_content = filter_aux[6]
        mult = filter_aux[7].replace("%20", " ").replace("messages", "").replace("message", "").replace("greater", "").replace("than", "")
       	filter_multiplicity_lower = 1
       	filter_multiplicity_upper = 2
       	multiplicity_having_condition = ""
        if mult.find("to") > 0:
        	filter_multiplicity_lower = mult.split('to')[0].strip()
        	filter_multiplicity_upper = mult.split('to')[1].strip()
        	if filter_multiplicity_upper == "any":
        		multiplicity_having_condition = " HAVING numEmails >= " + filter_multiplicity_lower
        	else:
        		multiplicity_having_condition = " HAVING numEmails >= " + filter_multiplicity_lower + " AND numEmails < " + filter_multiplicity_upper
        else:
        	filter_multiplicity_lower = mult.strip()
        	if int(mult.strip()) > 1:
        		multiplicity_having_condition = " HAVING numEmails >= " + filter_multiplicity_lower
        	else:
        		multiplicity_having_condition = " HAVING numEmails = " + filter_multiplicity_lower
    messageCounts = ", 0, 0, 0, 0 "
    cnx = mysql.connector.connect(user='commviz-user', database=database, password='commviz-user')
    cursor = cnx.cursor()
    if type_query is 1: #returning edges
        if filter_type=='yy':
            if filter_content != '':
                query = ("SELECT contents.id, sender, year, recipient,count(distinct messages.id) as numEmails FROM `messages` INNER JOIN contents ON messages.content=contents.content WHERE contents.id='"+filter_content+"' group by messages.content, sender, year, recipient" + multiplicity_having_condition)
            else:
                query = ("SELECT contents.id, sender, year, recipient,count(distinct messages.id) as numEmails FROM `messages` INNER JOIN contents ON messages.content=contents.content group by messages.content, sender, year, recipient" + multiplicity_having_condition)
        elif filter_type=='mm':
            if filter_content != '':
                query = ("SELECT contents.id, sender, month, recipient,count(distinct messages.id) as numEmails, year FROM `messages` INNER JOIN contents ON messages.content=contents.content WHERE year='"+filter_yy+"' and contents.id='"+filter_content+"' group by messages.content, sender, year, month, recipient" + multiplicity_having_condition)
            else:
                query = ("SELECT contents.id, sender, month, recipient,count(distinct messages.id) as numEmails, year FROM `messages` INNER JOIN contents ON messages.content=contents.content WHERE year='"+filter_yy+"' group by messages.content, sender, year, month, recipient" + multiplicity_having_condition)
        elif filter_type=='dd':
            if filter_content != '':
                query = ("SELECT contents.id, sender, day, recipient,count(distinct messages.id) as numEmails, year, month FROM `messages` INNER JOIN contents ON messages.content=contents.content WHERE year='"+filter_yy+"' and month='"+filter_mm+"' and contents.id='"+filter_content+"' group by messages.content, sender, year, month,day, recipient" + multiplicity_having_condition)
            else:
                query = ("SELECT contents.id, sender, day, recipient,count(distinct messages.id) as numEmails, year, month FROM `messages` INNER JOIN contents ON messages.content=contents.content WHERE year='"+filter_yy+"' and month='"+filter_mm+"' group by messages.content, sender, year, month,day, recipient" + multiplicity_having_condition)
        elif filter_type=='hh':
            if filter_content != '':
                query = ("SELECT contents.id, sender, `hour`, recipient,count(distinct messages.id) as numEmails, year, month, `day`  FROM `messages` INNER JOIN contents ON messages.content=contents.content WHERE year='"+filter_yy+"' and month='"+filter_mm+"' and day='"+filter_dd+"' and contents.id='"+filter_content+"' group by messages.content, sender, `year`, month, `day`, `hour`, recipient" + multiplicity_having_condition)
            else:
                query = ("SELECT contents.id, sender, `hour`, recipient,count(distinct messages.id) as numEmails, year, month, `day`  FROM `messages` INNER JOIN contents ON messages.content=contents.content WHERE year='"+filter_yy+"' and month='"+filter_mm+"' and day='"+filter_dd+"' group by messages.content, sender, `year`, month, `day`, `hour`, recipient" + multiplicity_having_condition)
        elif filter_type=='mi':
            if filter_content != '':
                query = ("SELECT contents.id, sender, `minute`, recipient,count(distinct messages.id) as numEmails, year, month, `day`, `hour` FROM `messages` INNER JOIN contents ON messages.content=contents.content WHERE year='"+filter_yy+"' and month='"+filter_mm+"' and day='"+filter_dd+"' and hour='"+filter_hh+"' and contents.id='"+filter_content+"' group by messages.content, sender, `year`, month, `day`, `hour`, `minute`, recipient" + multiplicity_having_condition)
            else:
                query = ("SELECT contents.id, sender, `minute`, recipient,count(distinct messages.id) as numEmails, year, month, `day`, `hour` FROM `messages` INNER JOIN contents ON messages.content=contents.content WHERE year='"+filter_yy+"' and month='"+filter_mm+"' and day='"+filter_dd+"' and hour='"+filter_hh+"' group by messages.content, sender, `year`, month, `day`, `hour`, `minute`, recipient" + multiplicity_having_condition)
    elif type_query is 2 :#returning nodes
        if filter_content != '':
            query = ("select people.id, package_group, axis.name as axis,people.userId" + messageCounts + " from people,timeframes,axis where timeframes.type = '" + filter_type + "' and axis.name in (3,4) union select people.id,contents.id,axis.name as axis, people.userId" + messageCounts + " from people,contents,axis where axis.name in (1,2) and contents.id='"+filter_content+"'")
        else:
            query = ("select people.id, package_group, axis.name as axis,people.userId" + messageCounts + " from people,timeframes,axis where timeframes.type = '" + filter_type + "' and axis.name in (3,4) union select people.id,contents.id,axis.name as axis, people.userId " + messageCounts + " from people,contents,axis where axis.name in (1,2) ")
    elif type_query is 3:#return single node by userID
            query = ("select people.id, package_group, axis.name as axis,people.userId from people,timeframes,axis where people.userId='"+person+"' and package_group='"+time+"' and timeframes.type = '" + filter_type + "' and axis.name in (3,4) union select people.id,contents.id,axis.name as axis, people.userId from people,contents,axis where people.userId='"+person+"' and axis.name in (1,2) and contents.id='"+contents+"'")
    elif type_query is 5: #return words contents
        query = ("select id,content from contents")
    elif type_query is 8: #return number of messages
    	userNumberClause = ", (COUNT(distinct sender)+COUNT(distinct recipient)) AS userNumber "
        if filter_type=='yy':
            if filter_content != '':
            	query = ("SELECT SUM(numEmails)" + userNumberClause + "from (SELECT sender, recipient, count(distinct messages.id) as numEmails FROM `messages` INNER JOIN contents ON messages.content=contents.content WHERE contents.id='"+filter_content+"' group by messages.content, sender, recipient" + multiplicity_having_condition + ") AS x")
            else:
            	query = ("SELECT SUM(numEmails)" + userNumberClause + "from (SELECT contents.id, sender, year, recipient, count(distinct messages.id) as numEmails FROM `messages` INNER JOIN contents ON messages.content=contents.content group by messages.content, sender, year, recipient" + multiplicity_having_condition + ") AS x")
        elif filter_type=='mm':
            if filter_content != '':
            	query = ("SELECT SUM(numEmails)" + userNumberClause + "from (SELECT contents.id, sender, month, recipient,count(distinct messages.id) as numEmails, year FROM `messages` INNER JOIN contents ON messages.content=contents.content WHERE year='"+filter_yy+"' and contents.id='"+filter_content+"' group by messages.content, sender, year, month, recipient" + multiplicity_having_condition + ") AS x")
            else:
            	query = ("SELECT SUM(numEmails)" + userNumberClause + "from (SELECT contents.id, sender, month, recipient,count(distinct messages.id) as numEmails, year FROM `messages` INNER JOIN contents ON messages.content=contents.content WHERE year='"+filter_yy+"' group by messages.content, sender, year, month, recipient" + multiplicity_having_condition + ") AS x")
        elif filter_type=='dd':
            if filter_content != '':
            	query = ("SELECT SUM(numEmails)" + userNumberClause + "from (SELECT contents.id, sender, day, recipient,count(distinct messages.id) as numEmails, year, month FROM `messages` INNER JOIN contents ON messages.content=contents.content WHERE year='"+filter_yy+"' and month='"+filter_mm+"' and contents.id='"+filter_content+"' group by messages.content, sender, year, month,day, recipient" + multiplicity_having_condition + ") AS x")
            else:
            	query = ("SELECT SUM(numEmails)" + userNumberClause + "from (SELECT contents.id, sender, day, recipient,count(distinct messages.id) as numEmails, year, month FROM `messages` INNER JOIN contents ON messages.content=contents.content WHERE year='"+filter_yy+"' and month='"+filter_mm+"' group by messages.content, sender, year, month,day, recipient" + multiplicity_having_condition + ") AS x")
        elif filter_type=='hh':
            if filter_content != '':
            	query = ("SELECT SUM(numEmails)" + userNumberClause + "from (SELECT contents.id, sender, `hour`, recipient,count(distinct messages.id) as numEmails, year, month, `day`  FROM `messages` INNER JOIN contents ON messages.content=contents.content WHERE year='"+filter_yy+"' and month='"+filter_mm+"' and day='"+filter_dd+"' and contents.id='"+filter_content+"' group by messages.content, sender, `year`, month, `day`, `hour`, recipient" + multiplicity_having_condition + ") AS x")
            else:
            	query = ("SELECT SUM(numEmails)" + userNumberClause + "from (SELECT contents.id, sender, `hour`, recipient,count(distinct messages.id) as numEmails, year, month, `day`  FROM `messages` INNER JOIN contents ON messages.content=contents.content WHERE year='"+filter_yy+"' and month='"+filter_mm+"' and day='"+filter_dd+"' group by messages.content, sender, `year`, month, `day`, `hour`, recipient" + multiplicity_having_condition + ") AS x")
        elif filter_type=='mi':
            if filter_content != '':
            	query = ("SELECT SUM(numEmails)" + userNumberClause + "from (SELECT contents.id, sender, `minute`, recipient,count(distinct messages.id) as numEmails, year, month, `day`, `hour` FROM `messages` INNER JOIN contents ON messages.content=contents.content WHERE year='"+filter_yy+"' and month='"+filter_mm+"' and day='"+filter_dd+"' and hour='"+filter_hh+"' and contents.id='"+filter_content+"' group by messages.content, sender, `year`, month, `day`, `hour`, `minute`, recipient" + multiplicity_having_condition + ") AS x")
            else:
            	query = ("SELECT SUM(numEmails)" + userNumberClause + "from (SELECT contents.id, sender, `minute`, recipient,count(distinct messages.id) as numEmails, year, month, `day`, `hour` FROM `messages` INNER JOIN contents ON messages.content=contents.content WHERE year='"+filter_yy+"' and month='"+filter_mm+"' and day='"+filter_dd+"' and hour='"+filter_hh+"' group by messages.content, sender, `year`, month, `day`, `hour`, `minute`, recipient" + multiplicity_having_condition + ") AS x")
    n=datetime.datetime.now()
    cursor.execute(query)
    results = cursor.fetchall()
    cursor.close()
    cnx.close()
    return results

class Node_vt: #Node class
    nodCount = 0
    def __init__(self, group, type, index, userId, sentCount_group, receivedCount_group, sentCount_overall, receivedCount_overall, nodeId):
        self.group = group
        self.type = type
        self.connectors = []
        self.index = index
        self.userId = userId
        self.sentCount_group = sentCount_group
        self.receivedCount_group = receivedCount_group
        self.sentCount_overall = sentCount_overall
        self.receivedCount_overall = receivedCount_overall
        self.nodeId = nodeId
        Node.nodCount += 1

    def displayCount_vt(self):
        print ("Total Nodes %d" % Node.nodCount)

    def displayNode_vt(self):
        print ("index: ",self.index,"User Id : ", self.userId,  ", Group: ", self.group, ", Type: ", self.type, ", # Connectors:",len(self.connectors))

class Edge_vt: #Edge class
    edgeCount = 0
    def __init__(self,content,time,source,target,num_email):
        self.content = content
        self.time = time
        self.source = source
        self.target = target
        self.degree = 0
        self.numEmails = num_email
        Edge.edgeCount += 1

    def displayCount_vt(self):
        print ("Total Edges %d" % Edge.edgeCount)

    def displayEdge_vt(self):
        print ("content: ", self.content, ", Time: ", self.time, ", Source: ", self.source.personId,"-",len(self.source.connectors), ", Target: ", self.target.personId,"-",len(self.target.connectors))

def get_key_vt(node):
    return "userid_" + str(node.userId) + "_group_" + str(node.group) + "_type_" + str(node.type)

def get_edge_vt(nodeList,sender_id,target_id,content,time,group_source,group_target,source_type,target_type,num_email):
	source = None
	target = None
	key_source = "userid_" + str(sender_id) + "_group_" + str(group_source) + "_type_" + str(source_type)
	key_target = "userid_" + str(target_id) + "_group_" + str(group_target) + "_type_" + str(target_type)
	if nodeList.get(key_source):
		source = nodeList.get(key_source)
	if nodeList.get(key_target):
		target = nodeList.get(key_target)
	if source != None:
		if target != None:
			return Edge_vt(content, time, source, target, num_email)
		else:
			return None
	else:
		return None
	#return edge

#return JSON string of Node
def getNodeJSON2_vt(node):
    string = "{" #opening object
    string += "\"nodeId\": " + str(node.nodeId) + ","
    string += "\"userId\": \"" + str(node.userId) + "\","
    string += "\"group\": \"" + str(node.group) + "\","
    string += "\"index\": " + str(node.index) + ","
    string += "\"sentCount_group\": " + str(node.sentCount_group) + ","
    string += "\"receivedCount_group\": " + str(node.receivedCount_group) + ","
    string += "\"sentCount_overall\": " + str(node.sentCount_overall) + ","
    string += "\"receivedCount_overall\": " + str(node.receivedCount_overall) + ","
    string += "\"type\": \"" + str(node.type) + "\""
    string += "}" #closing object
    return string

#get the contents
def get_json_string_contents_words_vt(database):
    words_contents=querying_db_vt(5,'',database)
    ite = 0
    stringcontentJson = "["
    for each_content in words_contents:
        ite += 1
        stringcontentJson += getContextJSON(each_content[0],each_content[1])
        if ite < len(words_contents):
             stringcontentJson += ","
    stringcontentJson += "]"
    return stringcontentJson

#query for all nodes and process the nodes
def get_nodes_vt(filter, database, contentlist, timelist):
    filter_aux = filter.split(',')
    nodes = []
    nodes = querying_db_vt(2, filter, database)
    string_nodes, nodeList = get_json_string_nodes_vt(nodes, contentlist, timelist)
    return string_nodes, nodeList

#query for one node and return the node
def get_single_node_vt(filter, database, person, time, contents):
    nodes = []
    nodes = querying_db_vt(3, filter, database, person=person, time=time, contents=contents)
    #nodes = querying_db_vt(4, filter, database, person=person, time=time, contents=contents)
    return nodes

#process the queried nodes and returns JSON node and the node list
def get_json_string_nodes_vt(nodes,contentlist=None,timelist=None):
    #generating the nodes
    nodeList = {}
    nodeListSort = []
    ite = 0
    s=datetime.datetime.now()
    for each_node in nodes:
        # filter the nodes that are not in the list.
        if  len(nodes)>10000 or timelist is None or contentlist is None or ((each_node[3],int(each_node[1])) in contentlist and each_node[2] in (1,2)) or ((each_node[3],int(each_node[1])) in timelist and each_node[2] in (3,4)):
            #newNode = Node_vt(each_node[1],each_node[2],0,each_node[3],ite)
            newNode = Node_vt(each_node[1], each_node[2], 0, each_node[3], each_node[4], each_node[5], each_node[6], each_node[7], ite)
            nodeAux = [get_key_vt(newNode),each_node[2],each_node[1],newNode.group]
            nodeListSort.append(nodeAux)
            nodeList[get_key_vt(newNode)] = newNode
            ite +=1
            
    e=datetime.datetime.now()
    print "time:",e-s
    #sort by userid and type
    nodeListSort.sort(key=lambda x: (int(x[1]),int(x[3])),reverse=False)

    ite = 0
    stringNodeJson = "["
    lastGroup = ""
    lastType = ""
    index = 0

    #calculate the message counts for each node
    #for each_node in nodeListSort:
    #    ite += 1

    #calculate the index, which determines the position in axis
    count = 0
    axis1_count = 0
    axis2_count = 0
    axis3_count = 0
    axis4_count = 0
    for each_node in nodeListSort:
    	if (each_node[1] == 1):
    	    axis1_count += 1
    	if (each_node[1] == 2):
    	    axis2_count += 1
    	if (each_node[1] == 3):
    	    axis3_count += 1
    	if (each_node[1] == 4):
    	    axis4_count += 1
    max_axis_count = 0
    if (axis1_count > max_axis_count):
    	max_axis_count = axis1_count
    if (axis2_count > max_axis_count):
    	max_axis_count = axis2_count
    if (axis3_count > max_axis_count):
    	max_axis_count = axis3_count
    if (axis4_count > max_axis_count):
    	max_axis_count = axis4_count
    
    for each_node in nodeListSort:
        ite += 1
        if unicode(each_node[1]) != unicode(lastType):
            index = 0 #starting from index=0 on each new axis
            lastType = each_node[1]
            lastGroup = each_node[2]
            nodeList[each_node[0]].index = index
        else:
            if unicode(each_node[2]) != unicode(lastGroup):
                lastGroup = each_node[2]
            	if (each_node[1] == 1):
                    index += max_axis_count/axis1_count
            	if (each_node[1] == 2):
                    index += max_axis_count/axis2_count
            	if (each_node[1] == 3):
                    index += max_axis_count/axis3_count
            	if (each_node[1] == 4):
                    index += max_axis_count/axis4_count
            else:
            	if (each_node[1] == 1):
                    index += max_axis_count/axis1_count
            	if (each_node[1] == 2):
                    index += max_axis_count/axis2_count
            	if (each_node[1] == 3):
                    index += max_axis_count/axis3_count
            	if (each_node[1] == 4):
                    index += max_axis_count/axis4_count
            nodeList[each_node[0]].index = index
        nodeList[each_node[0]].nodeId = count
        stringNodeJson += getNodeJSON2_vt(nodeList[each_node[0]])
        count += 1
        if ite < len(nodeListSort):
            stringNodeJson += ","

    stringNodeJson += "]" #closing object nodes
    return stringNodeJson, nodeList

#query and process all the information
def get_nodes_and_edges_vt(filter, database):
    start_time = datetime.datetime.now()
    print "START:", (start_time)

    edges_db = ''
    total_emails = 0
    total_users = 0
    edges_db = querying_db_vt(1, filter, database) #query for edges
    totals = querying_db_vt(8, filter, database) #query for number of messages
    total_emails = totals[0][0]
    total_users = totals[0][1]

    stringEdgeJson, stringNodeJson = get_json_string_edges_vt(edges_db, filter, database, total_users)#process edges
    stringcontentsJson = get_json_string_contents_words_vt(database)#process contents
    stringAuxJson = getAuxJSON(total_emails)#process number of messages

    #group all
    stringResult = "{ \"nodes\":"
    stringResult += stringNodeJson
    stringResult += ",\"links\":" + stringEdgeJson
    stringResult += ",\"contexts\":" + stringcontentsJson
    stringResult += ",\"totalEmails\":" + stringAuxJson
    stringResult += "}"
    end_time = datetime.datetime.now()
    print "END:", (end_time)
    print ("Duration of processing: ", end_time - start_time)
    return stringResult


def get_json_string_edges_vt(edges_db,filter,database,total_users):
    edges_list = []
    contentlist=[]
    timelist=[]
    nodes=[]
    # compare the number of edges with threshold,
    # to decide query all nodes and filter it,
    # or query all single nodes.
    if len(edges_db)>300:
        #query all nodes
        #generating the list of edges
        for each_email in edges_db:
            content = each_email[0] #0
            sender_id = each_email[1] #1
            time = each_email[2] #2
            target_id = each_email[3] #3
            if sender_id!='' and target_id!='' :
                #put node into nodelist for querying
                if (sender_id,content) not in contentlist :
                    contentlist.append((sender_id,content))
                if (sender_id,int(time)) not in timelist :
                    timelist.append((sender_id,int(time)))
                if (target_id,content) not in contentlist :
                    contentlist.append((target_id,content))
                if (target_id,int(time)) not in timelist :
                    timelist.append((target_id,int(time)))
        stringNodeJson, nodeList = get_nodes_vt(filter,database,contentlist,timelist)
        
    else:
        #query single nodes
        for each_email in edges_db:
            content = each_email[0] #0
            sender_id = each_email[1] #1
            time = each_email[2] #2
            target_id = each_email[3] #3
            num_emails = each_email[4] #4
            if sender_id!='' and target_id!='' and content != '' and time != '':
                nodes.extend(get_single_node_vt(filter,database,sender_id,str(time),str(content)))
                nodes.extend(get_single_node_vt(filter,database,target_id,str(time),str(content)))
        stringNodeJson, nodeList = get_json_string_nodes_vt(nodes)
        
    
    #add edge to edge list
    for each_email in edges_db:
        content = each_email[0] #0
        sender_id = each_email[1] #1
        time = each_email[2] #2
        target_id = each_email[3] #3
        num_emails = each_email[4] #4
        if sender_id != '' and target_id != '' and content != '' and time != '':
        	x13 = get_edge_vt(nodeList,sender_id,target_id,content,time,content,time,1,3,num_emails)
        	x43 = get_edge_vt(nodeList,sender_id,target_id,content,time,time,time,4,3,num_emails)
        	x12 = get_edge_vt(nodeList,sender_id,target_id,content,time,content,content,1,2,num_emails)
        	if x13:
        		edges_list.append(x13)
        	if x43:
        		edges_list.append(x43)
        	if x12:
        		edges_list.append(x12)
    
    ite = 0
    stringEdgeJson = "["
    for each_edge in edges_list:
        ite += 1
        stringEdgeJson += getEdgeJSON(each_edge)
        if ite < len(edges_list):
             stringEdgeJson += ","
    stringEdgeJson += "]" #closing object edges
    return stringEdgeJson,stringNodeJson
##Network vt ------------------------------------------------------------------------------------------------------------------
#query and process all the information
def get_nodes_and_edges_network_vt(filter, database, axis, index):
    start_time = datetime.datetime.now()
    print "START:", (start_time)
    edges_db = ''
    total_emails = 0
    total_users = 0
    edges_db = querying_db_vt(1, filter, database)
    totals = querying_db_vt(8, filter, database)
    total_emails = totals[0][0]
    total_users = totals[0][1]

    #process retrievaled edges and return nodes and edges JSON string.
    stringEdgeJson,stringNodeJson= get_json_string_edges_network_vt(edges_db,filter,database,axis,index)
    stringAuxJson = getAuxJSON(total_emails)

    stringResult = "{ \"nodes\":"
    stringResult += stringNodeJson
    stringResult += ",\"links\":" + stringEdgeJson
    stringResult += ",\"totalEmails\":"+ stringAuxJson
    stringResult += "}"
    end_time = datetime.datetime.now()
    print "END:", (end_time)
    print ("Duration of processing: ", end_time - start_time)
    return stringResult

#process the edges and nodes of network structure
def get_json_string_edges_network_vt(edges_db,filter,database,axis,index):
    edges_list = []
    G=nx.Graph()
    axisdict={}
    indexdict={}
    nodes=[]
    ranges=[]
    grouprange=[[],[],[]]
    #traverse all edges and add into graph
    for each_email in edges_db:
        content = each_email[0] #0
        sender_id = each_email[1] #1
        time = each_email[2] #2
        target_id = each_email[3] #3
        num_emails = each_email[4]
        G.add_edge(str(sender_id),str(target_id),weight=num_emails)
    #calculate CC DEG or PR value based on requirement
    if axis=='cc':
        axisdict=nx.clustering(G)
    elif axis=='deg':
        axisdict=nx.degree(G)
    elif axis=='pr':
        axisdict=nx.pagerank(G)
    if index=='cc':
        indexdict=nx.clustering(G)
    elif index=='deg':
        indexdict=nx.degree(G)
    elif index=='pr':
        indexdict=nx.pagerank(G)

    #get the range to divide axis
    if axis == 'cc':
        CCRANGE=[0.00000000001,0.9999999999]
        ranges=CCRANGE
    else:
        ranges=get_ranges(axisdict)

    #put each node on the axis
    for node in axisdict: 
        if axisdict[node]<ranges[0]:
            nodes.append([0,node,indexdict[node]])
            nodes.append([1,node,indexdict[node]])
            if indexdict[node] not in grouprange[0]:
                grouprange[0].append(indexdict[node])
        elif axisdict[node]>= ranges[0] and axisdict[node]<=ranges[1]:
            nodes.append([2,node,indexdict[node]])
            nodes.append([3,node,indexdict[node]])
            if indexdict[node] not in grouprange[1]:
                grouprange[1].append(indexdict[node])      
        elif axisdict[node]>ranges[1]:
            nodes.append([4,node,indexdict[node]])
            nodes.append([5,node,indexdict[node]])
            if indexdict[node] not in grouprange[2]:
                grouprange[2].append(indexdict[node])

    #get JSON string of nodes
    stringNodeJson, nodeList = get_json_string_nodes_network_vt(nodes,grouprange)

    #get JSON string of edges
    for each_email in edges_db:
        content = each_email[0] #0
        sender_id = each_email[1] #1
        time = each_email[2] #2
        target_id = each_email[3] #3
        num_emails = each_email[4] #4
        if sender_id!='' and target_id!='' and content != '' and time != '':

            #decide which two axis the edge connected
            if axisdict[sender_id]<ranges[0]:
                if axisdict[target_id]<ranges[0]:
                    edges_list.append(get_edge_network_vt(nodeList,sender_id,target_id,content,time,0,1,num_emails))
                elif axisdict[target_id]>=ranges[0] and axisdict[target_id]<=ranges[1]:
                    edges_list.append(get_edge_network_vt(nodeList,sender_id,target_id,content,time,1,2,num_emails))
                elif axisdict[target_id]>ranges[1]:
                    edges_list.append(get_edge_network_vt(nodeList,sender_id,target_id,content,time,0,5,num_emails))
            elif axisdict[sender_id]>= ranges[0] and axisdict[sender_id]<=ranges[1]:
                if axisdict[target_id]<ranges[0]:
                    edges_list.append(get_edge_network_vt(nodeList,sender_id,target_id,content,time,2,1,num_emails))
                elif axisdict[target_id]>=ranges[0] and axisdict[target_id]<=ranges[1]:
                    edges_list.append(get_edge_network_vt(nodeList,sender_id,target_id,content,time,2,3,num_emails))
                elif axisdict[target_id]>ranges[1]:
                    edges_list.append(get_edge_network_vt(nodeList,sender_id,target_id,content,time,3,4,num_emails))
            elif axisdict[sender_id]>ranges[1]:
                if axisdict[target_id]<ranges[0]:
                    edges_list.append(get_edge_network_vt(nodeList,sender_id,target_id,content,time,5,0,num_emails))
                elif axisdict[target_id]>= ranges[0] and axisdict[target_id]<=ranges[1]:
                    edges_list.append(get_edge_network_vt(nodeList,sender_id,target_id,content,time,4,3,num_emails))
                elif axisdict[target_id]>ranges[1]:
                    edges_list.append(get_edge_network_vt(nodeList,sender_id,target_id,content,time,4,5,num_emails))
                
    ite = 0
    stringEdgeJson = "["
    for each_edge in edges_list:
        ite += 1
        stringEdgeJson += getEdgeJSON(each_edge)
        if ite < len(edges_list):
             stringEdgeJson += ","
    stringEdgeJson += "]" #closing object edges
    return stringEdgeJson,stringNodeJson

def get_json_string_nodes_network_vt(nodes,grouprange):
    #generating the nodes
    nodeList = {}
    nodeListSort = []
    ite = 0
    colorrange=color_range(grouprange)
    for each_node in nodes:
        newNode = Node_network_vt(get_color(each_node[2],colorrange[each_node[0]/2]),each_node[0],0,each_node[1],ite,each_node[2])
        nodeAux = [get_key_network(newNode), each_node[0], each_node[2]]
        nodeListSort.append(nodeAux)
        nodeList[get_key_network(newNode)] = newNode
        ite +=1
    nodeListSort.sort(key=lambda x: (int(x[1]),float(x[2])),reverse=False)

    ite = 0
    stringNodeJson = "["
    lastGroup = ""
    lastType = ""
    index = 0

#    count = 0
#    for each_node in nodeListSort:
#        ite += 1
#        if unicode(each_node[1]) != unicode(lastType):
#            index = 0
#            lastType = each_node[1]
#            lastGroup = each_node[2]
#            nodeList[each_node[0]].index = index
#        else:
#            if unicode(each_node[2]) != unicode(lastGroup):
#                lastGroup = each_node[2]
#                index += 2
#            else:
#                index += 1
#            nodeList[each_node[0]].index = index
#
#        nodeList[each_node[0]].nodeId = count
#        stringNodeJson += getNodeJSON_network(nodeList[each_node[0]])
#        count += 1
#        if ite < len(nodeListSort):
#            stringNodeJson += ","

    count = 0
    axis1_count = 0
    axis2_count = 0
    axis3_count = 0
    axis4_count = 0
    axis5_count = 0
    axis6_count = 0
    for each_node in nodeListSort:
    	if (each_node[1] == 0):
    	    axis1_count += 1
    	if (each_node[1] == 1):
    	    axis2_count += 1
    	if (each_node[1] == 2):
    	    axis3_count += 1
    	if (each_node[1] == 3):
    	    axis4_count += 1
    	if (each_node[1] == 4):
    	    axis5_count += 1
    	if (each_node[1] == 5):
    	    axis6_count += 1
    max_axis_count = 0
    if (axis1_count > max_axis_count):
    	max_axis_count = axis1_count
    if (axis2_count > max_axis_count):
    	max_axis_count = axis2_count
    if (axis3_count > max_axis_count):
    	max_axis_count = axis3_count
    if (axis4_count > max_axis_count):
    	max_axis_count = axis4_count
    if (axis5_count > max_axis_count):
    	max_axis_count = axis5_count
    if (axis6_count > max_axis_count):
    	max_axis_count = axis6_count
    
    for each_node in nodeListSort:
        ite += 1
        if unicode(each_node[1]) != unicode(lastType):
            index = 0 #starting from index=0 on each new axis
            lastType = each_node[1]
            lastGroup = each_node[2]
            nodeList[each_node[0]].index = index
        else:
            if unicode(each_node[2]) != unicode(lastGroup):
                lastGroup = each_node[2]
            	if (each_node[1] == 01):
                    index += max_axis_count/axis1_count
            	if (each_node[1] == 1):
                    index += max_axis_count/axis2_count
            	if (each_node[1] == 2):
                    index += max_axis_count/axis3_count
            	if (each_node[1] == 3):
                    index += max_axis_count/axis4_count
            	if (each_node[1] == 4):
                    index += max_axis_count/axis5_count
            	if (each_node[1] == 5):
                    index += max_axis_count/axis6_count
            else:
            	if (each_node[1] == 0):
                    index += max_axis_count/axis1_count
            	if (each_node[1] == 1):
                    index += max_axis_count/axis2_count
            	if (each_node[1] == 2):
                    index += max_axis_count/axis3_count
            	if (each_node[1] == 3):
                    index += max_axis_count/axis4_count
            	if (each_node[1] == 4):
                    index += max_axis_count/axis5_count
            	if (each_node[1] == 5):
                    index += max_axis_count/axis6_count
            nodeList[each_node[0]].index = index
        nodeList[each_node[0]].nodeId = count
        stringNodeJson += getNodeJSON_network(nodeList[each_node[0]])
        count += 1
        if ite < len(nodeListSort):
            stringNodeJson += ","
    stringNodeJson += "]" #closing object nodes
    return stringNodeJson, nodeList

#node class in network structure
class Node_network_vt:
    nodCount = 0
    def __init__(self, group, type, index, userId, nodeId, value):
        self.group = group
        self.type = type
        self.connectors = []
        self.index = index
        self.userId=userId
        self.nodeId = nodeId
        self.value=value
        Node.nodCount += 1
        
def get_key_network_vt(node):
    return "userid_" + str(node.userId) + "_type_" + str(node.type)

def get_edge_network_vt(nodeList,sender_id,target_id,content,time,source_type,target_type,num_email):
    key_source = "userid_" + str(sender_id) + "_type_" + str(source_type)
    source = nodeList[key_source]
    key_target = "userid_" + str(target_id) + "_type_" + str(target_type)
    target = nodeList[key_target]
    edge = Edge_vt(content,time,source,target,num_email)
    return edge

@app.route("/vt")
@crossdomain(origin='*')
def get_data_vt():
    filter = request.args.get('filter')
    database=request.args.get('db')
    if filter!='':
        json_result = get_nodes_and_edges_vt(filter, database)
    return json_result

@app.route("/vtnetwork")
@crossdomain(origin='*')
def get_datas_vt():
    filter = request.args.get('filter')
    database = request.args.get('db')
    axis = request.args.get('axis')
    index = request.args.get('position')
    if filter != '':
        json_result = get_nodes_and_edges_network_vt(filter, database, axis, index)
    return json_result
#vt: for vast and twitter
#End of VAST and Twitter---------------------------------------------------------------------------------------------------

#This decides the route of the API
@app.route("/cv")
@crossdomain(origin='*')
def get_data():
    filter = request.args.get('filter')
    database=request.args.get('db')
    if filter!='':
        cnx = mysql.connector.connect(user='commviz-user', database=database, password='commviz-user')
        cursor = cnx.cursor()
        json_result = get_nodes_and_edges(cursor, filter)
        cursor.close()
        cnx.close()
    return json_result

@app.route("/cvnetwork")
@crossdomain(origin='*')
def get_datas():
    filter = request.args.get('filter')
    database=request.args.get('db')
    axis=request.args.get('axis')
    index=request.args.get('position')
    if filter!='':
        cnx = mysql.connector.connect(user='commviz-user', database=database, password='commviz-user')
        cursor = cnx.cursor()
        json_result = get_nodes_and_edges_network(cursor, filter, axis, index)
        cursor.close()
        cnx.close()
    return json_result
            
if __name__ == '__main__' :
  app.run()
