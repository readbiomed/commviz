import mysql.connector
import datetime
from dateutil.parser import parse
import csv
from bs4 import BeautifulSoup

#----------------------------------------------------------------
timestamp_c='Timestamp' #header of timestamp
source_c='from' #header of source
target_c='to' #header of target
content_c='location' #header of content
database='commvizBSP' #database name
csvfile='filter.csv' #csvfile name and path
username='commviznew' #database username
password='commviznew' #database password
hiveplot_html_path="html/new_hive_plot.html" #hive plot html file path"
network_html_path="html/network_hive_plot.html" #network hive plot html file path"

#This is all the attributes that user need to modify-------------------

def edit_html(htmlfile):
    dblist=[]
    f=open(htmlfile)
    soup=BeautifulSoup(f)
    f.close()
    for option in soup.find_all("option"):
        dblist.append(str(option.get_text().strip()))
    new_tag=soup.new_tag("option")
    new_tag.string=database
    if database not in dblist:
        new_tag=soup.new_tag("option")
        new_tag.string=database
        soup.find('select').insert(-1,new_tag)
        html= soup.prettify("utf-8",formatter=None)
        with open(htmlfile, "wb") as f:
            f.write(html)


def create_database(dbname):
    cnx = mysql.connector.connect(user=username,password=password)
    cursor = cnx.cursor()
    try:
        cursor.execute(
            "CREATE DATABASE {} DEFAULT CHARACTER SET 'utf8'".format(dbname))
    except mysql.connector.Error:
        pass
    
def polish_database(database):
    cnx = mysql.connector.connect(user=username, database=database, password=password)
    cursor = cnx.cursor()

    #messages
    query0 = ("DROP TABLE IF EXISTS `messages`")
    cursor.execute(query0)

    query1 = ("CREATE TABLE `messages` (`id` bigint(20) NOT NULL,  `content` text NOT NULL,  `sender` text NOT NULL,  `year` text NOT NULL,  `month` text NOT NULL,  `day` text NOT NULL,  `time` text NOT NULL,  `hour` text NOT NULL,  `minute` text NOT NULL,  `recipient` text NOT NULL  ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1")
    cursor.execute(query1)

    query2 = ("ALTER TABLE `messages`ADD PRIMARY KEY (`id`);")
    cursor.execute(query2)

    query3=("ALTER TABLE `messages`MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;")
    cursor.execute(query3)

    #people
    queryP0 = ("DROP TABLE IF EXISTS `people`")
    cursor.execute(queryP0)
    
    queryP1 = ("CREATE TABLE `people` (`id` int(11) NOT NULL,  `userId` text NOT NULL) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;")
    cursor.execute(queryP1)

    queryP2 = ("ALTER TABLE `people`ADD PRIMARY KEY (`id`);")
    cursor.execute(queryP2)

    queryP3=("ALTER TABLE `people` MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;")
    cursor.execute(queryP3)

    
    #contents
    queryTo0 = ("DROP TABLE IF EXISTS `contents`")
    cursor.execute(queryTo0)

    queryC1 = ("CREATE TABLE `contents` (`id` int(11) NOT NULL,  `content` text NOT NULL) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;")
    cursor.execute(queryC1)

    queryC2 = ("ALTER TABLE `contents`ADD PRIMARY KEY (`id`);")
    cursor.execute(queryC2)

    queryC3=("ALTER TABLE `contents` MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;")
    cursor.execute(queryC3)

    #Timeframes
    queryT0 = ("DROP TABLE IF EXISTS `timeframes`;")
    cursor.execute(queryT0)

    queryT1 = ("CREATE TABLE `timeframes` (`id` int(11) NOT NULL,  `package_group` text NOT NULL,  `type` text NOT NULL) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;")
    cursor.execute(queryT1)

    queryT2 = ("ALTER TABLE `timeframes` ADD PRIMARY KEY (`id`);")
    cursor.execute(queryT2)

    queryT3 = ("ALTER TABLE `timeframes`MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;")
    cursor.execute(queryT3)

    #axis
    queryA0 = ("DROP TABLE IF EXISTS `axis`;")
    cursor.execute(queryA0)

    queryA1 = ("CREATE TABLE `axis` (`id` int(11) NOT NULL,  `name` int(11) NOT NULL) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;")
    cursor.execute(queryA1)

    queryA2 = ("ALTER TABLE `axis` ADD PRIMARY KEY (`id`);")
    cursor.execute(queryA2)

    queryA3 = ("ALTER TABLE `axis`MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;")
    cursor.execute(queryA3)

    cnx.commit()
    cursor.close()
    cnx.close()

def insert_message(content, sender, year, month, day, full_time, hour, minute, recipient):
    cnx = mysql.connector.connect(user=username, database=database, password=password)
    cursor = cnx.cursor()

    query = ("INSERT INTO messages VALUES (null,'"+content+"','"+sender+"','"+year+"','"+month
             +"','"+day+"','"+full_time+"','"+hour+"','"+minute+"','"+recipient+"')")
    cursor.execute(query)
    cnx.commit()
    cursor.close()
    cnx.close()

def insert_timeframe(value,typ):
    #typ: yy,mm,dd,hh,mi
    cnx = mysql.connector.connect(user=username, database=database, password=password)
    cursor = cnx.cursor()

    query = ("INSERT INTO timeframes VALUES (null,'"+str(value)+"','"+str(typ)+"')")
    cursor.execute(query)
    cnx.commit()
    cursor.close()
    cnx.close()

def insert_people(people):
    #this function is not tested jet
    cnx = mysql.connector.connect(user=username, database=database, password=password)
    cursor = cnx.cursor()
    query=("INSERT INTO people VALUES (null,'"+str(people)+"')")
    cursor.execute(query)

    cnx.commit()
    cursor.close()
    cnx.close()

def insert_contents():
    #this function is not tested jet
    cnx = mysql.connector.connect(user=username, database=database, password=password)
    cursor = cnx.cursor()

    query = ("insert into contents(content) select content from messages group by content;")
    cursor.execute(query)

    cnx.commit()
    cursor.close()
    cnx.close()

def insert_axis():
    cnx = mysql.connector.connect(user=username, database=database, password=password)
    cursor = cnx.cursor()
    for n in range(1,5):
        query = ("INSERT INTO axis VALUES (null,'"+str(n)+"')")
        cursor.execute(query)
    cnx.commit()
    cursor.close()
    cnx.close()



def csv2mysql(csvfile,delimiter=','):
    start_time = datetime.datetime.now()
    print start_time
    #read from CSV file
    csvconv=csv.DictReader(open(csvfile,'r'),delimiter=delimiter)
    #clean the database
    create_database(database)
    polish_database(database)
    contents =[]
    monthset=set()
    dayset=set()
    hourset=set()
    timeFrameByYYYY = []
    timeFrameByYYYYMonth = []
    timeFrameMonth=[]
    timeFrameByYYYYMonthDD = []
    timeFrameDay=[]
    timeFrameByHour=[]
    people = []

    #[mes.Id	content	weight	sen.Id	year	month	fullDate	rec.Id]
    for each_line in csvconv:
        #parse the timestamp to standard datetime format
        datetimes=parse(each_line[timestamp_c])
        year=str(datetimes.year)
        month=str(datetimes.month)
        day=str(datetimes.day)
        full_time=str(datetimes.time())
        hour=str(datetimes.hour)
        minute=str(datetimes.minute)
        source=each_line[source_c]
        target=each_line[target_c]
        content=each_line[content_c]
        #insert the message into database
        insert_message(content,source,year,month,day,full_time,hour,minute,target)

        #to get each time period.
        if year!='':
            if year not in timeFrameByYYYY:
                timeFrameByYYYY.append(year)

        if year!=''and month!='':
            temp_ym = year +'/'+month
            if temp_ym not in timeFrameByYYYYMonth:
                timeFrameByYYYYMonth.append(temp_ym)
                monthset.add(month)
                
        if year!=''and month!='' and day!='':
            temp_ymd = year + '/' +month + '/' + day
            if temp_ymd not in timeFrameByYYYYMonthDD:
                timeFrameByYYYYMonthDD.append(temp_ymd)
                dayset.add(day)
                
        if hour!='':
            if hour not in timeFrameByHour:
                hourset.add(hour)
                
        if content!='':
            if content not in contents:
                contents.append(content)

        if source!='':
            if source not in people:
                people.append(source)

        if target!='':
            if target not in people:
                people.append(target)
                
    #insert people into the database.
    for person in people:
        insert_people(person)
    #insert contents into the database.
    insert_contents()
    #insert axis
    insert_axis()

    for each_year in timeFrameByYYYY:
        #insert the timeframe into the database
        insert_timeframe(each_year,'yy')

    for each_month in sorted(monthset):
        insert_timeframe(each_month,'mm')

    for each_day in sorted(dayset):
        insert_timeframe(each_day,'dd')

    for each_hour in sorted(hourset):
        insert_timeframe(each_hour,'hh')
        
    for n in range(00,60):
        insert_timeframe(n,'mi')
        
    edit_html(hiveplot_html_path)
    edit_html(network_html_path)
    end_time = datetime.datetime.now()
    print end_time
    print "Duration of processing: ", end_time - start_time

    
if __name__=='__main__':
    edit_html(hiveplot_html_path)
    edit_html(network_html_path)
    #csv2mysql(csvfile)
