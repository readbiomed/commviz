package mallettopicmodeller;

import cc.mallet.pipe.*;
import cc.mallet.pipe.iterator.CsvIterator;
import cc.mallet.topics.ParallelTopicModel;
import cc.mallet.types.Alphabet;
import cc.mallet.types.FeatureSequence;
import cc.mallet.types.IDSorter;
import cc.mallet.types.Instance;
import cc.mallet.types.InstanceList;
import cc.mallet.types.LabelSequence;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.Iterator;
import java.util.Locale;
import java.util.TreeSet;

//help: http://mallet.cs.umass.edu/topics-devel.php
/**
 *
 * @author bofoghi
 */
public class TopicModeller {

    public static void doLDA_streaming(String stopListFileName, int numTopics, String outputJSONFile) {
        try {
            InstanceList instances = new InstanceList(createPipe(true, true, true, stopListFileName));
            instances = MySQLReader.readInstanceFromDB_enron(instances);

            ParallelTopicModel model = new ParallelTopicModel(numTopics, 1.0, 0.01);
            model.addInstances(instances);
            model.setNumThreads(5);
            //model.setNumIterations(10);
            model.setNumIterations(1500);
            model.estimate();

            FileWriter fw = new FileWriter(outputJSONFile);
            BufferedWriter bw = new BufferedWriter(fw);
            model.topicXMLReport(new PrintWriter(bw), 20);

            int indexInstanceIterator = 0;
            Iterator<Instance> instanceIterator = instances.iterator();

            bw.write("mes.Id\ttopic\tweight\tsen.Id\tyear\tmonth\tfullDate\trec.Id" + "\n");
            while (instanceIterator.hasNext()) {
                Instance currInstance = instanceIterator.next();
                double[] topicDistribution = model.getTopicProbabilities(indexInstanceIterator);
                int maximalTopicIndex = getIndexMaxArray(topicDistribution);
                Formatter out = new Formatter(new StringBuilder(), Locale.US);
                EmailDate emailDate = MySQLReader.getEmailDate_enron((Integer) currInstance.getName());
                if (emailDate != null) {
                    out.format("%d\t%d\t%.3f\t%d\t%d\t%s\t%s\t%s\t",
                            currInstance.getName(),
                            maximalTopicIndex,
                            topicDistribution[maximalTopicIndex],
                            currInstance.getSource(),
                            emailDate.year,
                            emailDate.month,
                            emailDate.dateString,
                            MySQLReader.getRecipientIds_enron((Integer) currInstance.getName()));
                } else {
                    out.format("%d\t%d\t%.3f\t%d\t%s\t%s\t%s\t%s\t",
                            currInstance.getName(),
                            maximalTopicIndex,
                            topicDistribution[maximalTopicIndex],
                            currInstance.getSource(),
                            " ",
                            " ",
                            " ",
                            MySQLReader.getRecipientIds_enron((Integer) currInstance.getName()));
                }

                bw.write(out.toString() + "\n");
                System.out.println(indexInstanceIterator++ + " of " + instances.size());
            }
            bw.close();
            fw.close();
        } catch (FileNotFoundException fnfex) {
        } catch (UnsupportedEncodingException ueex) {
        } catch (IOException ioex) {
        }
    }

    public static void doLDA_batch(String inputFile, String stopListFile, int numTopics) {
        try {
            InstanceList instances = new InstanceList(createPipe(true, true, true, stopListFile));
            instances.addThruPipe(new CsvIterator(new FileReader(inputFile), java.util.regex.Pattern.compile("^(\\S*)[\\s,]*(\\S*)[\\s,]*(.*)$"), 3, 2, 1)); // data, label, name fields

            ParallelTopicModel model = new ParallelTopicModel(numTopics, 1.0, 0.01);
            model.addInstances(instances);
            model.setNumThreads(2);
            model.setNumIterations(1500);
            model.estimate();

            Alphabet dataAlphabet = instances.getDataAlphabet();
            ArrayList<TreeSet<IDSorter>> topicSortedWords = model.getSortedWords();

            int indexInstanceIterator = 0;
            Iterator<Instance> instanceIterator = instances.iterator();
            while (instanceIterator.hasNext()) {
                Instance currInstance = instanceIterator.next();
                FeatureSequence tokens = (FeatureSequence) currInstance.getData();
                LabelSequence topics = model.getData().get(indexInstanceIterator).topicSequence;

                Formatter out = new Formatter(new StringBuilder(), Locale.US);
                for (int position = 0; position < tokens.getLength(); position++) {
                    out.format("%s-%d ", dataAlphabet.lookupObject(tokens.getIndexAtPosition(position)), topics.getIndexAtPosition(position));
                }
                System.out.println(out);

                double[] topicDistribution = model.getTopicProbabilities(indexInstanceIterator);
                int maximalTopicIndex = getIndexMaxArray(topicDistribution);

                Iterator<IDSorter> iterator = topicSortedWords.get(maximalTopicIndex).iterator();
                out = new Formatter(new StringBuilder(), Locale.US);
                out.format("%d\t%.3f\t", maximalTopicIndex, topicDistribution[maximalTopicIndex]);
                int rank = 0;

                while (rank < 5) {
                    IDSorter idCountPair = iterator.next();
                    out.format("%s (%.0f) ", dataAlphabet.lookupObject(idCountPair.getID()), idCountPair.getWeight());
                    rank++;
                }
                System.out.println(out);
                System.out.println("");
                indexInstanceIterator++;
            }
        } catch (FileNotFoundException fnfex) {
        } catch (UnsupportedEncodingException ueex) {
        } catch (IOException ioex) {
        }
    }

    private static int getIndexMaxArray(double[] inp) {
        int maxInd = 0;
        if (inp != null) {
            double max = inp[0];
            for (int i = 0; i < inp.length; i++) {
                if (inp[i] > max) {
                    maxInd = i;
                }
            }
        }

        return maxInd;
    }

    private static Pipe createPipe(boolean lowerCase, boolean removeStopWords, boolean removeNonAlphabetic, String stopListFileName) {
        ArrayList<Pipe> pipes = new ArrayList();

        pipes.add(new CharSequence2TokenSequence("[\\p{L}\\p{N}_]+"));
        if (lowerCase) {
            pipes.add(new TokenSequenceLowercase());
        }
        if (removeStopWords) {
            java.io.File stopListFile = new java.io.File(stopListFileName);
            pipes.add(new TokenSequenceRemoveStopwords(stopListFile, "UTF-8", false, false, false));
        }

        if (removeNonAlphabetic) {
            pipes.add(new TokenSequenceRemoveNonAlpha());
        }

        pipes.add(new TokenSequence2FeatureSequence());
        return new SerialPipes(pipes);
    }
}
