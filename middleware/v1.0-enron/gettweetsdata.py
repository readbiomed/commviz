import couchdb
import csv
import time
from BeautifulSoup import BeautifulSoup
 
def stripAllTags(html):
        if html is None:
                return None
        return ''.join(BeautifulSoup(html).findAll(text = True))
    
def getTextAndGeo():
    t1 = time.time()
    contents={}
    people={}
    for row in db.view('ccc/getMasterData',limit=950000):
            #limit indicates the number of raw tweets
        temp={}
        if(row.value['reply'] is not None and (row.value['time'].endswith("3") or row.value['time'].endswith("4") or row.value['time'].endswith("5"))):
        #Just get the tweets with reply, and in 2013-2015
            temp['id']=row.value['id']
            temp['time']=row.value['time']
            temp['reply']=row.value['reply']
            temp['from']=row.value['from']
            try: #count tweet number of each user
                    people[str(row.value['reply'])]+=1
            except:
                    people[str(row.value['reply'])]=1
            try:
                    people[str(row.value['from'])]+=1
            except:
                    people[str(row.value['from'])]=1
            #remove the html tags    
            content=stripAllTags(row.value['content']).encode('utf8')
            temp['content']=content
            try:
                    contents[content]+=1
            except:
                    contents[content]=1
            csvlist.append(temp)
    #delete all tweets that the user tweets less
    for d,x in contents.items():
            if x<len(csvlist)/100:
                    del contents[d]
    for d,x in people.items():
            if x<len(csvlist)/2000:
                    del people[d]
                    
    newlist=[]
    for item in csvlist:
        if item['from'] in people.keys() and item['reply'] in people.keys() and item['content'] in contents.keys():
                newlist.append(item)
    print len(newlist),'newlist'
    #save into csv file
    with open('twitter.csv', 'wb') as f:
        keys=newlist[0].keys()
        w = csv.DictWriter(f, keys)
        w.writeheader()
        w.writerows(newlist)
    t2 = time.time()
    print (t2 - t1)

couch = couchdb.Server('http://ccc2015:montreal@115.146.93.71:5984/')
db = couch['tweets']
csvlist=[]
getTextAndGeo()
