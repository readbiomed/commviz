/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mallettopicmodeller;

/**
 *
 * @author bofoghi
 */
public class Term_with_LL implements Comparable {

    public String term;
    public double ll;
    public LL_tfd1_tfd2 ll_tfd1_tfd2;

    public Term_with_LL(String _term, double _ll, LL_tfd1_tfd2 _ll_tfd1_tfd2) {
        this.term = _term;
        this.ll = _ll;
        this.ll_tfd1_tfd2 = new LL_tfd1_tfd2();
        this.ll_tfd1_tfd2.ll = _ll;
        this.ll_tfd1_tfd2.tfd1 = _ll_tfd1_tfd2.tfd1;
        this.ll_tfd1_tfd2.tfd2 = _ll_tfd1_tfd2.tfd2;
    }

    @Override
    public int compareTo(Object obj) {
        Term_with_LL termWithLL = (Term_with_LL) obj;

        if (this.ll < termWithLL.ll) {
            return 1;
        } else if (this.ll > termWithLL.ll) {
            return -1;
        } else {
            return 0;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Term_with_LL)) {
            return false;
        }

        Term_with_LL termWithLL = (Term_with_LL) obj;
        return this.ll == termWithLL.ll;
    }
}
