/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mallettopicmodeller;

/**
 *
 * @author bofoghi
 */
public class Term_with_LL_Comparator implements java.util.Comparator<Term_with_LL> {

    @Override
    public int compare(Term_with_LL tll1, Term_with_LL tll2) {
        return tll1.compareTo(tll2);
    }
}
