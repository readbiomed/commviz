package mallettopicmodeller;

import cc.mallet.types.Instance;
import cc.mallet.types.InstanceList;
import com.sun.javafx.scene.layout.region.Margins;
import java.sql.*;

/**
 *
 * @author bofoghi
 */
public class MySQLReader {

    final static String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    final static String DB_URL = "jdbc:mysql://localhost:3306/enron";
    final static String UN = "root";
    final static String PW = "password";

    public static InstanceList readInstanceFromDB_enron(InstanceList instances) {
        try {
            Connection conn = null;

            Class.forName(JDBC_DRIVER);
            System.out.println("MySQL JDBC driver loaded: OK!");

            conn = DriverManager.getConnection(DB_URL, UN, PW);
            conn.setAutoCommit(false);

            System.out.println("Connected to MySQL db: OK!");

            Statement stmt = conn.createStatement();
            stmt.setFetchSize(Integer.MIN_VALUE);

            System.out.println("Data reading in progress...");
//            ResultSet rs = stmt.executeQuery("select bodies.body, messages.senderid, messages.messageid from"
//                    + " bodies join messages on bodies.messageid = messages.messageid limit 10");

            ResultSet rs = stmt.executeQuery("select bodies.body, people.company_based_id, messages.messageid from "
                    + "((bodies join messages on bodies.messageid = messages.messageid) join people on people.personid = messages.senderid)");

            int ind = 1;
            Lemmatizer lemmatizer = new Lemmatizer();

            while (rs.next()) {
                //1: message body
                //2: senderid, now from company_based_id
                //3: messageid

                Instance ins = new Instance(CleanDocument.clean(rs.getString(1), lemmatizer), "target", rs.getInt(3), rs.getInt(2));
                if (instanceQualifies(ins)) {
                    instances.addThruPipe(ins);
                }
                System.out.println(ind++);
            }
            rs.close();
            stmt.close();
            conn.close();
            System.out.println("Data reading completed: OK!");
        } catch (Exception e) {
            System.out.println("Reading from DB was not successful.");
            System.out.println(e);
        }

        return instances;
    }

    private static boolean instanceQualifies(Instance instance) {
        if (Integer.parseInt(instance.getSource().toString()) >= 2) {
            return true;
        } else {
            return false;
        }
    }

    public static String getRecipientIds_enron(int messageId) {
        String res = "";
        try {
            Connection conn = null;

            Class.forName(JDBC_DRIVER);
            conn = DriverManager.getConnection(DB_URL, UN, PW);
            Statement stmt = conn.createStatement();
//            ResultSet rs = stmt.executeQuery("select recipients.personid from \n"
//                    + "recipients join messages on recipients.messageid = messages.messageid\n"
//                    + "where messages.messageid = " + messageId);

            ResultSet rs = stmt.executeQuery("select people.company_based_id from"
                    + " ((recipients join messages on recipients.messageid = messages.messageid) join people "
                    + "on people.personid = recipients.personid) where messages.messageid =" + messageId);

            while (rs.next()) {
                res += Integer.toString(rs.getInt(1)) + ",";
            }
            rs.close();
            stmt.close();
            conn.close();
            System.out.println("Data reading completed: OK!");
        } catch (Exception e) {
            System.out.println("Reading from DB was not successful.");
            System.out.println(e);
        }
        if (!res.equalsIgnoreCase("")) {
            res = res.substring(0, res.length() - 1);
        }
        return res;
    }

    public static EmailDate getEmailDate_enron(int messageId) {
        String res = "";
        try {
            Connection conn = null;

            Class.forName(JDBC_DRIVER);
            conn = DriverManager.getConnection(DB_URL, UN, PW);
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("select headervalue\n"
                    + "from headers where \n"
                    + "(messageid = " + messageId + ") and (headername = \"Date\")");

            while (rs.next()) {
                res = rs.getString(1);
            }
            rs.close();
            stmt.close();
            conn.close();
        } catch (Exception e) {
            System.out.println("Reading from DB was not successful.");
            System.out.println(e);
            return null;
        }
        if (!res.equalsIgnoreCase("")) {
            return parseEmailDateString(res.trim());
        } else {
            return null;
        }
    }

    private static EmailDate parseEmailDateString(String _str) {
        //example_str: Wed, 31 Oct 2001 05:23:56 -0800 (PST)
        //example_str: Sun, 13 Jan 2002 05:36:53 -0800 (PST)
        try {
            if (_str.split(" ").length >= 3) {
                return new EmailDate(Integer.parseInt(_str.split(" ")[3]), _str.split(" ")[2], _str);
            } else {
                return null;
            }
        } catch (Exception e) {
            return null;
        }
    }
}
