# README #

This is the repository for development of Visualisation tools for Network Communication Data.

Developed by the team headed by Karin Verspoor at the University of Melbourne, for a project funded by the Australian Defence Science and Technology Group (DST Group).

The CommViz tool implementation for the Enron dataset is available at: <http://commviz.eng.unimelb.edu.au>

Further extensions (reflecting some improvements to the speed and alternative data sets) are available at: <http://commviz.eng.unimelb.edu.au/new_hive_plot.html>
