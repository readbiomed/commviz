package mallettopicmodeller;

/**
 *
 * @author bofoghi
 */
public class CleanDocument {
    public static String clean(String body, Lemmatizer lemmatizer){
        
        body = body.replaceAll(".+@.+", "");
        body = body.replaceAll("To:.+\\n", "");
        body = body.replaceAll("cc:.+\\n", "");
        body = body.replaceAll("CC:.+\\n", "");
        body = body.replaceAll("BCC:.+\\n", "");
        body = body.replaceAll("bcc:.+\\n", "");
        body = body.replaceAll("Subject:.+\\n", "");
        body = body.replaceAll("From:.+\\n", "");
        body = body.replaceAll("Sent:.+\\n", "");
        body = body.replaceAll("Received:.+\\n", "");
        body = body.replaceAll("Content-Type:.+\\n", "");
        body = body.replaceAll("Reply- Organization:.+\\n", "");
        body = body.replaceAll("Date:.+\\n", "");
        body = body.replaceAll("X-Mailer:.+\\n", "");
        body = body.replaceAll("MIME-Version:.+\\n", "");
        body = body.replaceAll("((http(s?)://www.)|(http(s?)://)|(www.))[a-z|.|/|A-Z|0-9|_]*", "");
        
        body = body.replaceAll("\\n", " ");
        body = body.replaceAll(">", "");
        body = body.replaceAll("<", "");
        body = body.replaceAll("--+.+--+", "");
        body = body.replaceAll("\\*\\*+.+\\*\\*+", "");
        body = body.replaceAll(" __+.+__+", "");
        body = body.replaceAll("==+.+==+", "");
        
        body = lemmatizer.lemmatize_string(body);
        return body;
    }
}
