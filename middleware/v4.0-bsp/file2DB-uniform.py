from dateutil.parser import parse
from bs4 import BeautifulSoup
import mysql.connector
import datetime
import csv
import sys

def edit_html_v3(htmlfile, username, password, database, update):
    cnx = mysql.connector.connect(user=username, password=password)
    cursor = cnx.cursor()
    f = open(htmlfile)
    soup = BeautifulSoup(f, "html.parser")
    f.close()
    
    if (update == 'True'):
    	#add all dbs which will include the new one
    	dbSelectTag = soup.find('select', id='database')
    	for option in dbSelectTag.find_all("option"):
    		option.replace_with('')
    	query = ("show databases like \"cv%\"")
    	cursor.execute(query)		
    	for (db,) in cursor:
    		new_tag = soup.new_tag("option")
    		new_tag.string = db
    		soup.find('select', id='database').append(new_tag)
    		#html = soup.prettify("utf-8", formatter=None)
    else:
    	#only add the new db and if it does not exist
    	isInOptions = False
    	dbSelectTag = soup.find('select', id='database')
    	for option in dbSelectTag.find_all("option"):
    		if (option.text.lower().strip() == database.lower().strip()):
    			isInOptions = True
    			break
    	if (isInOptions == False):
    		new_tag = soup.new_tag("option")
    		new_tag.string = database
    		dbSelectTag.append(new_tag)
    html = soup.prettify("utf-8", formatter=None)
    with open(htmlfile, "wb") as f:
    	f.write(html)

def edit_html_v2(htmlfile, username, password):
    f = open(htmlfile)
    soup = BeautifulSoup(f, "html.parser")
    f.close()
    dbSelectTag = soup.find('select', id='database')
    for option in dbSelectTag.find_all("option"):
		option.replace_with('')
    cnx = mysql.connector.connect(user=username, password=password)
    cursor = cnx.cursor()
    query = ("show databases like \"cv%\"")
    cursor.execute(query)		
    for (db,) in cursor:
    	new_tag = soup.new_tag("option")
    	new_tag.string = db
        soup.find('select', id='database').append(new_tag)
        html = soup.prettify("utf-8", formatter=None)
	with open(htmlfile, "wb") as f:
		f.write(html)

def create_database(dbname, username, password):
    cnx = mysql.connector.connect(user=username,password=password)
    cursor = cnx.cursor()
    try:
        cursor.execute(
            "CREATE DATABASE {0} DEFAULT CHARACTER SET 'utf8'".format(dbname))
    except mysql.connector.Error:
        pass
    
def polish_database(database, username, password):
    cnx = mysql.connector.connect(user=username, database=database, password=password)
    cursor = cnx.cursor()

    #communication
    query0 = ("DROP TABLE IF EXISTS `communication`")
    query1 = ("CREATE TABLE `communication` (`id` bigint(20) NOT NULL, `context` text NOT NULL, `sender` text NOT NULL, `year` text NOT NULL, `month` text NOT NULL,  `day` text NOT NULL, `time` text NOT NULL, `hour` text NOT NULL, `minute` text NOT NULL, `recipient` text NOT NULL) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1")
    query2 = ("ALTER TABLE `communication` ADD PRIMARY KEY (`id`);")
    query3=("ALTER TABLE `communication` MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;")
    cursor.execute(query0)
    cursor.execute(query1)
    cursor.execute(query2)
    cursor.execute(query3)

    #communicator
    queryP0 = ("DROP TABLE IF EXISTS `communicator`")
    queryP1 = ("CREATE TABLE `communicator` (`id` int(11) NOT NULL, `communicatorId` text NOT NULL, `role` text NOT NULL) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;")
    #role can be: s: for sender, r: for recipien, sr: for sender and recipient
    queryP2 = ("ALTER TABLE `communicator` ADD PRIMARY KEY (`id`);")
    queryP3=("ALTER TABLE `communicator` MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;")
    cursor.execute(queryP0)
    cursor.execute(queryP1)
    cursor.execute(queryP2)
    cursor.execute(queryP3)
    
    #context
    queryTo0 = ("DROP TABLE IF EXISTS `context`")
    queryC1 = ("CREATE TABLE `context` (`id` int(11) NOT NULL, `value` text NOT NULL) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;")
    queryC2 = ("ALTER TABLE `context` ADD PRIMARY KEY (`id`);")
    cursor.execute(queryTo0)
    cursor.execute(queryC1)
    cursor.execute(queryC2)

    #Timeframe
    queryT0 = ("DROP TABLE IF EXISTS `timeframe`;")
    queryT1 = ("CREATE TABLE `timeframe` (`value` text NOT NULL, `type` text NOT NULL) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;")
    cursor.execute(queryT0)
    cursor.execute(queryT1)

    #axis
    queryA0 = ("DROP TABLE IF EXISTS `axis`;")
    queryA1 = ("CREATE TABLE `axis` (`id` int(11) NOT NULL, `name` int(11) NOT NULL) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;")
    queryA2 = ("ALTER TABLE `axis` ADD PRIMARY KEY (`id`);")
    queryA3 = ("ALTER TABLE `axis`MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;")
    cursor.execute(queryA0)
    cursor.execute(queryA1)
    cursor.execute(queryA2)
    cursor.execute(queryA3)

    cnx.commit()
    cursor.close()
    cnx.close()

def insert_communication(cursor, context, sender, year, month, day, full_time, hour, minute, recipient):
    query = ("INSERT INTO communication VALUES (null,'"+context+"','"+sender+"','"+year+"','"+month
             +"','"+day+"','"+full_time+"','"+hour+"','"+minute+"','"+recipient+"')")
    cursor.execute(query)

def insert_timeframe(cursor, value, type):
    query = ("INSERT INTO timeframe VALUES ('"+str(value)+"','"+str(type)+"')")
    cursor.execute(query)

def insert_communicator(cursor, communicatorId, role):
    query=("INSERT INTO communicator VALUES (null,'"+str(communicatorId)+"','"+role+"')")
    cursor.execute(query)

def update_communicator(cursor, communicatorId, newRole):
    query=("UPDATE communicator SET role='" + newRole + "' WHERE communicatorId=" + communicatorId)
    cursor.execute(query)

def insert_context(cursor, context):
    query = ("INSERT INTO context VALUES ('" + context.id + "','" + context.value + "')")
    cursor.execute(query)

def insert_axis(cursor):
    for n in range(1,5):
        query = ("INSERT INTO axis VALUES (null,'"+str(n)+"')")
        cursor.execute(query)

class Context:
    def __init__(self, id, value):
        self.id = id
        self.value = value

def csv2mysql(csvDataFile, delimiter, username, password, database, source_c, target_c, contextId_c, contextValue_c, timestamp_c, hiveplot_html_path, network_html_path, update):
    #read from the csv files
    csvData = csv.DictReader(open(csvDataFile, 'r'), delimiter=delimiter)
    
    #clean the database
    create_database(database, username, password)
    polish_database(database, username, password)
    
    #initialize necessary variables
    monthset = set()
    dayset = set()
    hourset = set()
    timeFrameByYYYY = []
    timeFrameByYYYYMonth = []
    timeFrameMonth = []
    timeFrameByYYYYMonthDD = []
    timeFrameDay = []
    timeFrameByHour = []
    sources = []
    targets = []
    contexts = []

    ind = 0
    cnx = mysql.connector.connect(user=username, database=database, password=password)
    cursor = cnx.cursor()
    for each_line in csvData:
        datetimes = parse(each_line[timestamp_c], dayfirst=True)
        year = str(datetimes.year)
        month = str(datetimes.month)
        day = str(datetimes.day)
        full_time = str(datetimes.time())
        hour = str(datetimes.hour)
        minute = str(datetimes.minute)
        source = each_line[source_c]
        target = each_line[target_c]
        contextId = each_line[contextId_c]
        contextValue = each_line[contextValue_c]
        
        #insert communication in to database
        insert_communication(cursor, contextId, source, year, month, day, full_time, hour, minute, target)

        #get time period
        if year != '':
            if year not in timeFrameByYYYY:
                timeFrameByYYYY.append(year)

        if year != ''and month != '':
            temp_ym = year +'/'+month
            if temp_ym not in timeFrameByYYYYMonth:
                timeFrameByYYYYMonth.append(temp_ym)
                monthset.add(month)
                
        if year != ''and month != '' and day != '':
            temp_ymd = year + '/' + month + '/' + day
            if temp_ymd not in timeFrameByYYYYMonthDD:
                timeFrameByYYYYMonthDD.append(temp_ymd)
                dayset.add(day)
                
        if hour != '':
            if hour not in timeFrameByHour:
                hourset.add(hour)

        if source != '':
            if source not in sources:
                sources.append(source)

        if target != '':
            if target not in targets:
                targets.append(target)

        if contextId != '':
            if (not any(str(int(context.id)-1) == contextId for context in contexts)):
            	new_context = Context(str(int(contextId)+1), contextValue)
            	contexts.append(new_context)
        
        sys.stdout.write("reading data csv line: %d \r" %(ind) )
        sys.stdout.flush()
        ind += 1
    print "reading data csv line: ", ind, " successful."
    print 'communications inserted in to database successfully.'
    
    #insert communicators in to the database.
    for source in sources:
        insert_communicator(cursor, source, 's')
    for target in targets:
    	if target not in sources:
    		insert_communicator(cursor, target, 'r')
    	else:
    		update_communicator(cursor, target, 'sr')
    print 'communicators inserted in to database successfully.'

    for context in contexts:
        insert_context(cursor, context)
    print 'contexts inserted in to database successfully.'

    #insert axis
    insert_axis(cursor)
    print 'axes inserted in to database successfully.'

    #insert timeframes in to the database
    for each_year in timeFrameByYYYY:
        insert_timeframe(cursor, each_year,'yy')
    for each_month in sorted(monthset):
        insert_timeframe(cursor, each_month,'mm')
    for each_day in sorted(dayset):
        insert_timeframe(cursor, each_day,'dd')
    for each_hour in sorted(hourset):
        insert_timeframe(cursor, each_hour,'hh')
    for n in range(00,60):
        insert_timeframe(cursor, n,'mi')
    
    print 'timeframes inserted in to database successfully.'
    cnx.commit()
    cursor.close()
    cnx.close()

    print 'changes to the database were committed successfully.'
    edit_html_v3(hiveplot_html_path, username, password, database, update)
    edit_html_v3(network_html_path, username, password, database, update)

def main():
    if len(sys.argv) < 13:
    	print 'Usage: python file2DB-uniform.py [timestamp-h] [source-h] [target-h] [data-context-h] [dbName] [csvDataFile] [mysqlUsername] [mysqlPassword] [hiveHTML] [networkHTML] [update]'
    	print '[timestamp-h]: column header in csvDataFile referring to the timestamp'
    	print '[source-h]: column header in csvDataFile referring to the source'
    	print '[target-h]: column header in csvDataFile referring to the target'
    	print '[contextId-h]: column header in csvDataFile referring to the context id'
    	print '[contextValue-h]: column header in csvDataFile referring to the context value'
    	print '[dbName]: the name of the database to be created'
    	print '[csvDataFile]: the input csv file containing communication data'
    	print '[mysqlUsername]: username for the MySql database management system'
    	print '[mysqlPassword]: password for the MySql database management system'
        print '[hiveHTML]: the html file of the output hive plot of communications'
        print '[networkHTML]: the html file of the output network structure of communications'
        print '[update: True will update the list of databases in the html files]'
        sys.exit(0)
    else:
    	start_time = datetime.datetime.now()
    	print "start time: ", start_time
    	delimiter = ','
        
        timestamp_c = sys.argv[1] #communication timestamp: header in csv
        source_c = sys.argv[2] #communication source: header in csv
        target_c = sys.argv[3] #communication target: header in csv
        contextId_c = sys.argv[4] #communication context: header in csv
        contextValue_c = sys.argv[5] #communication context: header in csv
        database = sys.argv[6] #the name of the database to be created
        csvDataFile = sys.argv[7] #input csv file
        username = sys.argv[8] #mysql username
        password = sys.argv[9] #mysql password
        hiveplot_html_path = sys.argv[10] #hiveplot html file path
        network_html_path = sys.argv[11] #hiveplot network html file path
        update = sys.argv[12] #whether to update the db list in the hiveplot and network html files
        
        csv2mysql(csvDataFile, delimiter, username, password, database, source_c, target_c, contextId_c, contextValue_c, timestamp_c, hiveplot_html_path, network_html_path, update)
        end_time = datetime.datetime.now()
        print "end time: ", end_time

if __name__=='__main__':
    main()