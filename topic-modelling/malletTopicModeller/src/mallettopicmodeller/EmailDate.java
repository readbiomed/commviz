package mallettopicmodeller;

/**
 *
 * @author bofoghi
 */
public class EmailDate {
    public int year;
    public String month;
    public String dateString;
    
    public EmailDate(int _year, String _month, String _dateString){
        this.year = _year;
        this.month = _month;
        this.dateString = _dateString;
    }
}
