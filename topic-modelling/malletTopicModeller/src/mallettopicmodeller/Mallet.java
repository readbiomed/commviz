package mallettopicmodeller;

/**
 *
 * @author bofoghi
 */
public class Mallet {

    public static void main(String[] args) {
        //String stopListFile = "C:\\Users\\bofoghi\\Desktop\\fromUSB-HARD\\PROJECTS\\JAVA\\mallet-2.0.7\\stoplists\\en.txt";
        String stopListFile = "C:\\Users\\bofoghi\\MY-FILES\\NOTEPAD\\vanRijsbergenSTOPWORDS-no-single-letter.txt";
        String topicModellingExampleInputFile = "C:\\Users\\bofoghi\\MY-FILES\\PROJECTS\\JAVA\\malletClassifier\\ap.txt";
        String enronTopicModellingOutputFile = "C:\\Users\\bofoghi\\MY-FILES\\PROJECTS\\JAVA\\malletTopicModeller\\enron.txt";
        boolean lowerCase = false;
        boolean removeStopWords = false;


        TopicModeller.doLDA_streaming(stopListFile, 20, enronTopicModellingOutputFile);
        //was 50 topics before: ref: http://www.uoguelph.ca/~wdarling/tm/enron.html
              
        System.out.println("done...");
    }
}
